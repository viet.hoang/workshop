#define parameters
Param(
	[string]$EngineSuffix = 'Sc9',
	[string]$Prefix = 'sc903com',
	[string]$CommerceOpsSiteName = 'CommerceOps_',
	[string]$CommerceShopsSiteName = 'CommerceShops_',
	[string]$CommerceAuthoringSiteName = 'CommerceAuthoring_',
	[string]$CommerceMinionsSiteName = 'CommerceMinions_',
	[string]$SitecoreBizFxSiteName = 'SitecoreBizFx',
	[string]$SitecoreIdentityServerSiteName = 'SitecoreIdentityServer',
	[string]$SolrService = 'Solr_6.6.2',
	[string]$PathToSolr = 'D:\solr-6.6.2\',
	[string]$SqlServer = 'DESKTOP-XXXXXXX',
	[string]$SqlAccount = 'sa',
	[string]$SqlPassword = 'password'
)
#Write-TaskHeader function modified from SIF
Function Write-TaskHeader {
    param(
        [Parameter(Mandatory=$true)]
        [string]$TaskName,
        [Parameter(Mandatory=$true)]
        [string]$TaskType
    )

    function StringFormat {
        param(
            [int]$length,
            [string]$value,
            [string]$prefix = '',
            [string]$postfix = '',
            [switch]$padright
        )

        # wraps string in spaces so we reduce length by two
        $length = $length - 2 #- $postfix.Length - $prefix.Length
        if($value.Length -gt $length){
            # Reduce to length - 4 for elipsis
            $value = $value.Substring(0, $length - 4) + '...'
        }

        $value = " $value "
        if($padright){
            $value = $value.PadRight($length, '*')
        } else {
            $value = $value.PadLeft($length, '*')
        }

        return $prefix + $value + $postfix
    }

    $actualWidth = (Get-Host).UI.RawUI.BufferSize.Width
    $width = $actualWidth - ($actualWidth % 2)
    $half = $width / 2

    $leftString = StringFormat -length $half -value $TaskName -prefix '[' -postfix ':'
    $rightString = StringFormat -length $half -value $TaskType -postfix ']' -padright

    $message = ($leftString + $rightString)
    Write-Host ''
    Write-Host $message -ForegroundColor 'Red'
}

Function Remove-Service{
	[CmdletBinding()]
	param(
		[string]$serviceName
	)
	if(Get-Service "My Service" -ErrorAction SilentlyContinue){
		sc.exe delete $serviceName
	}
}

Function Remove-Website{
	[CmdletBinding()]
	param(
		[string]$siteName		
	)

	$appCmd = "C:\windows\system32\inetsrv\appcmd.exe"
	& $appCmd delete site $siteName
}

Function Remove-AppPool{
	[CmdletBinding()]
	param(		
		[string]$appPoolName
	)

	$appCmd = "C:\windows\system32\inetsrv\appcmd.exe"
	& $appCmd delete apppool $appPoolName
}

#Stop Solr Service
Write-TaskHeader -TaskName "Solr Services" -TaskType "Stop"
Write-Host "Stopping solr service"
Stop-Service $SolrService -Force -ErrorAction stop
Write-Host "Solr service stopped successfully"

#Delete solr cores
Write-TaskHeader -TaskName "Solr Services" -TaskType "Delete Cores"
Write-Host "Deleting Solr Cores"
$pathToCores = "$pathToSolr\server\solr\*Scope"
Remove-Item $pathToCores -recurse -force -ErrorAction stop
Write-Host "Solr Cores deleted successfully"

#Remove Sites and App Pools from IIS
Write-TaskHeader -TaskName "Internet Information Services" -TaskType "Remove Websites"


Write-Host "Deleting Website $CommerceOpsSiteName$EngineSuffix"
Remove-Website -siteName $CommerceOpsSiteName$EngineSuffix -ErrorAction silentlycontinue
Write-Host "Websites deleted"

Write-Host "Deleting Website $CommerceShopsSiteName$EngineSuffix"
Remove-Website -siteName $CommerceShopsSiteName$EngineSuffix -ErrorAction silentlycontinue
Write-Host "Websites deleted"

Write-Host "Deleting Website $CommerceAuthoringSiteName$EngineSuffix"
Remove-Website -siteName $CommerceAuthoringSiteName$EngineSuffix -ErrorAction silentlycontinue
Write-Host "Websites deleted"

Write-Host "Deleting Website $CommerceMinionsSiteName$EngineSuffix"
Remove-Website -siteName $CommerceMinionsSiteName$EngineSuffix  -ErrorAction silentlycontinue
Write-Host "Websites deleted"

Write-Host "Deleting Website $SitecoreBizFxSiteName$EngineSuffix"
Remove-Website -siteName $SitecoreBizFxSiteName -ErrorAction silentlycontinue
Write-Host "Websites deleted"

Write-Host "Deleting Website $SitecoreIdentityServerSiteName$EngineSuffix"
Remove-Website -siteName $SitecoreIdentityServerSiteName -ErrorAction silentlycontinue
Write-Host "Websites deleted"



Remove-AppPool -appPoolName $CommerceOpsSiteName$EngineSuffix -ErrorAction silentlycontinue
Write-Host "Application pools deleted"
Remove-AppPool -appPoolName $CommerceShopsSiteName$EngineSuffix -ErrorAction silentlycontinue
Write-Host "Application pools deleted"
Remove-AppPool -appPoolName $CommerceAuthoringSiteName$EngineSuffix -ErrorAction silentlycontinue
Write-Host "Application pools deleted"
Remove-AppPool -appPoolName $CommerceMinionsSiteName$EngineSuffix -ErrorAction silentlycontinue
Write-Host "Application pools deleted"
Remove-AppPool -appPoolName $SitecoreBizFxSiteName -ErrorAction silentlycontinue
Write-Host "Application pools deleted"
Remove-AppPool -appPoolName $SitecoreIdentityServerSiteName -ErrorAction silentlycontinue


Remove-Item C:\inetpub\wwwroot\$CommerceOpsSiteName$EngineSuffix -recurse -force -ErrorAction silentlycontinue
Write-Host "Websites removed from wwwroot"
Remove-Item C:\inetpub\wwwroot\$CommerceShopsSiteName$EngineSuffix -recurse -force -ErrorAction silentlycontinue
Write-Host "Websites removed from wwwroot"
Remove-Item C:\inetpub\wwwroot\$CommerceAuthoringSiteName$EngineSuffix -recurse -force -ErrorAction silentlycontinue
Write-Host "Websites removed from wwwroot"
Remove-Item C:\inetpub\wwwroot\$CommerceMinionsSiteName$EngineSuffix -recurse -force -ErrorAction silentlycontinue
Write-Host "Websites removed from wwwroot"
Remove-Item C:\inetpub\wwwroot\$SitecoreBizFxSiteName -recurse -force -ErrorAction silentlycontinue
Write-Host "Websites removed from wwwroot"
Remove-Item C:\inetpub\wwwroot\$SitecoreIdentityServerSiteName -recurse -force -ErrorAction silentlycontinue
Write-Host "Websites removed from wwwroot"


Write-TaskHeader -TaskName "SQL Server" -TaskType "Drop Databases"
#Drop databases from SQL
Write-Host "Dropping databases from SQL server"
push-location
import-module sqlps

Write-Host $("Dropping database SitecoreCommerce9_Global")
$commerceDbPrefix = $("DROP DATABASE IF EXISTS [SitecoreCommerce9_Global]")
Write-Host $("Query: $($commerceDbPrefix)")
invoke-sqlcmd -ServerInstance $SqlServer -U $SqlAccount -P $SqlPassword -Query $commerceDbPrefix -ErrorAction silentlycontinue

Write-Host $("Dropping database [SitecoreCommerce9_SharedEnvironments]")
$sharedDbPrefix = $("DROP DATABASE IF EXISTS [SitecoreCommerce9_SharedEnvironments]")
Write-Host $("Query: $($sharedDbPrefix)")
invoke-sqlcmd -ServerInstance $SqlServer -U $SqlAccount -P $SqlPassword -Query $sharedDbPrefix -ErrorAction silentlycontinue


Write-Host "Databases dropped successfully"
pop-location

#Start Solr up again
Write-TaskHeader -TaskName "Solr Services" -TaskType "Start"
Start-Service $SolrService -ErrorAction stop
Write-Host "Solr Services restarted, environment is clean to reinstall"