For more information, please read the blog posts:
- https://buoctrenmay.wordpress.com/2017/03/01/an-approach-for-automating-sitecore-deployments-with-teamcity-at-your-machine/
- https://buoctrenmay.wordpress.com/2017/02/24/workshop-automating-sitecore-deployments-with-teamcity-at-your-machine-part-1/
- https://buoctrenmay.wordpress.com/2017/02/24/workshop-automating-sitecore-deployments-with-teamcity-at-your-machine-part-2/