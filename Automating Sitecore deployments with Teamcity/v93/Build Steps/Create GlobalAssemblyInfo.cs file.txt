if (Test-Path .\GlobalAssemblyInfo.cs)
{
 remove-Item .\GlobalAssemblyInfo.cs
}

$content = '
using System.Reflection;

[assembly: AssemblyCompany("VietHoang")]
[assembly: AssemblyCopyright("Copyright © VietHoang 2020")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyVersion("{0}")]
[assembly: AssemblyInformationalVersion("{0}")]
[assembly: AssemblyFileVersion("{0}")]
' -f "%vhs.AssemblyVersion%"

new-item -path .\ -name GlobalAssemblyInfo.cs -type "file" -value $content

Get-Content .\GlobalAssemblyInfo.cs