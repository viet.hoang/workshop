$WakeUp = {
    Param ([string] $url)
    try 
    {
        Write-Output "Waking up $url ..."
        $client = new-object system.net.WebClient
        $client.UseDefaultCredentials = $true
        $null = $client.OpenRead($url)
        $client.Dispose()
        Write-Output "$url Ok"
    } catch {
        Write-Error $_
        throw $_
    }
}

$jobs = @()
# Run your code that needs to be elevated here
@(
    "http://%vhs.deployIisAppPath_CM%/",
    "http://%vhs.deployIisAppPath_CD1%/",
    "http://%vhs.deployIisAppPath_CD2%/"
 ) | % { 
    $jobs += Start-Job -ScriptBlock $WakeUp -ArgumentList $_ 
    }

Receive-Job -Job $jobs -Keep
Wait-Job -Job $jobs