$gulpConfigPath = ".\gulp-config.js"
(Get-Content $gulpConfigPath).replace('%vhs.deployIisAppPath_CD1%', '%vhs.deployIisAppPath_CD2%') | Set-Content $gulpConfigPath
(Get-Content $gulpConfigPath).replace('MSDeployServiceURL: "%vhs.msDeployServiceUrl_CD1%"', 'MSDeployServiceURL: "%vhs.msDeployServiceUrl_CD2%"') | Set-Content $gulpConfigPath
(Get-Content $gulpConfigPath).replace('isCD: false', 'isCD: true') | Set-Content $gulpConfigPath

Get-Content $gulpConfigPath