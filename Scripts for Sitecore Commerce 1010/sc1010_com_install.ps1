#Requires -Version 3
param(
    # The root folder of Sitecore instances
	[string]$ScInstallDirectory = "C:\websites",
    # The root folder with WDP files.
    [string]$XCInstallRoot = "..",
    # The root folder of SIF.Sitecore.Commerce package.
    [string]$XCSIFInstallRoot = $PWD,
    # Specifies whether or not to bypass the installation of the default SXA Storefront. By default, the Sitecore XC installation script also deploys the SXA Storefront.
    [bool]$SkipInstallDefaultStorefront = $false,
    # Specifies whether or not to bypass the installation of the SXA Storefront packages.
    # If set to $true, $TasksToSkip parameter will be populated with the list of tasks to skip in order to bypass SXA Storefront packages installation.
    [bool]$SkipDeployStorefrontPackages = $false,

    # Path to the Master_SingleServer.json file provided in the SIF.Sitecore.Commerce package.
    [string]$Path = "$XCSIFInstallRoot\Configuration\Commerce\Master_SingleServer.json",
    # Path to the Commerce Solr schemas provided as part of the SIF.Sitecore.Commerce package.
    [string]$SolrSchemas = "$XCSIFInstallRoot\SolrSchemas",
    # Path to the SiteUtilityPages folder provided as part of the SIF.Sitecore.Commerce package.
    [string]$SiteUtilitiesSrc = "$XCSIFInstallRoot\SiteUtilityPages",
    # Path to the location where you downloaded the Microsoft.Web.XmlTransform.dll file.
    [string]$MergeToolFullPath = "$XCInstallRoot\Microsoft.Web.XmlTransform.dll",
    # Path to the Adventure Works Images.OnPrem SCWDP file
    [string]$AdventureWorksImagesWdpFullPath = "$XCInstallRoot\Adventure Works Images.OnPrem.scwdp.zip",
    # Path to the Sitecore Commerce Connect Core SCWDP file.
    [string]$CommerceConnectWdpFullPath = "$XCInstallRoot\Sitecore Commerce Connect Core*.scwdp.zip",
    # Path to the Sitecore Commerce Engine Connect OnPrem SCWDP file.
    [string]$CEConnectWdpFullPath = "$XCInstallRoot\Sitecore Commerce Engine Connect*.scwdp.zip",
    # Path to the Sitecore Commerce Experience Accelerator SCWDP file.
    [string]$SXACommerceWdpFullPath = "$XCInstallRoot\Sitecore Commerce Experience Accelerator*.scwdp.zip",
    # Path to the Sitecore Commerce Experience Accelerator Habitat Catalog SCWDP file.
    [string]$SXAStorefrontCatalogWdpFullPath = "$XCInstallRoot\Sitecore Commerce Experience Accelerator Habitat*.scwdp.zip",
    # Path to the Sitecore Commerce Experience Accelerator Storefront SCWDP file.
    [string]$SXAStorefrontWdpFullPath = "$XCInstallRoot\Sitecore Commerce Experience Accelerator Storefront*.scwdp.zip",
    # Path to the Sitecore Commerce Experience Accelerator Storefront Themes SCWDP file.
    [string]$SXAStorefrontThemeWdpFullPath = "$XCInstallRoot\Sitecore Commerce Experience Accelerator Storefront Themes*.scwdp.zip",
    # Path to the Sitecore Commerce Experience Analytics Core SCWDP file.
    [string]$CommercexAnalyticsWdpFullPath = "$XCInstallRoot\Sitecore Commerce ExperienceAnalytics Core*.scwdp.zip",
    # Path to the Sitecore Commerce Experience Profile Core SCWDP file.
    [string]$CommercexProfilesWdpFullPath = "$XCInstallRoot\Sitecore Commerce ExperienceProfile Core*.scwdp.zip",
    # Path to the Sitecore Commerce Marketing Automation Core SCWDP file.
    [string]$CommerceMAWdpFullPath = "$XCInstallRoot\Sitecore Commerce Marketing Automation Core*.scwdp.zip",
    # Path to the Sitecore Commerce Marketing Automation for AutomationEngine zip file.
    [string]$CommerceMAForAutomationEngineZIPFullPath = "$XCInstallRoot\Sitecore Commerce Marketing Automation for AutomationEngine*.zip",
    # Path to the Sitecore Experience Accelerator zip file.
    [string]$SXAModuleZIPFullPath = "$XCInstallRoot\Sitecore Experience Accelerator*.zip",
    # Path to the Sitecore.PowerShell.Extensions zip file.
    [string]$PowerShellExtensionsModuleZIPFullPath = "$XCInstallRoot\Sitecore.PowerShell.Extensions*.zip",
    # Path to the Sitecore BizFx Server SCWDP file.
    [string]$BizFxPackage = "$XCInstallRoot\Sitecore.BizFx.OnPrem*scwdp.zip",
    # Path to the Commerce Engine Service SCWDP file.
    [string]$CommerceEngineWdpFullPath = "$XCInstallRoot\Sitecore.Commerce.Engine.OnPrem.Solr.*scwdp.zip",
    # Path to the Sitecore.Commerce.Habitat.Images.OnPrem SCWDP file.
    [string]$HabitatImagesWdpFullPath = "$XCInstallRoot\Sitecore.Commerce.Habitat.Images.OnPrem.scwdp.zip",

    # The prefix that will be used on SOLR, Website and Database instances. The default value matches the Sitecore XP default.
    [string]$SiteNamePrefix = "storefront1010",
    # The prefix to match Marketing Automation engine service name. Used in form "<MAEnginePrefix>_xconnect-MarketingAutomationService".
    [string]$MAEnginePrefix = $SiteNamePrefix,
    # The name of the Sitecore site instance.
    [string]$SiteName = "$SiteNamePrefix.local",
    # Identity Server site name.
    [string]$IdentityServerSiteName = "$SiteNamePrefix.IdentityServer.local",
    # The url of the Sitecore Identity server.
    [string]$SitecoreIdentityServerUrl = "https://$IdentityServerSiteName",
    # The Commerce Engine Connect Client Id for the Sitecore Identity Server
    [string]$CommerceEngineConnectClientId = "CommerceEngineConnect",
    # The Commerce Engine Connect Client Secret for the Sitecore Identity Server
    [string]$CommerceEngineConnectClientSecret = "xuWXlh1TjGt91Hh7APW1xuel2DuCQqmNX7a69jVfaMw=",
    # The host header name for the Sitecore storefront site.
    [string]$SiteHostHeaderName = "storefront1010.local",

    # The path of the Sitecore XP site.
    [string]$InstallDir = "$ScInstallDirectory\$SiteName",
    # The path of the Sitecore XConnect site.
    [string]$XConnectInstallDir = "$ScInstallDirectory\$SiteNamePrefix.xconnect.local",
    # The path to the inetpub folder where Commerce is installed.
    [string]$CommerceInstallRoot = "$ScInstallDirectory",

    # The prefix for Sitecore core and master databases.
    [string]$SqlDbPrefix = $SiteNamePrefix,
    # The location of the database server where Sitecore XP databases are hosted. In case of named SQL instance, use "SQLServerName\\SQLInstanceName"
    [string]$SitecoreDbServer = $($Env:COMPUTERNAME),
    # The name of the Sitecore core database.
    [string]$SitecoreCoreDbName = "$($SqlDbPrefix)_Core",
    # A SQL user with sysadmin privileges.
    [string]$SqlUser = "sa",
    # The password for $SQLAdminUser.
    [string]$SqlPass = "Abcd1234",

    # The name of the Sitecore domain.
    [string]$SitecoreDomain = "sitecore",
    # The name of the Sitecore user account.
    [string]$SitecoreUsername = "admin",
    # The password for the $SitecoreUsername.
    [string]$SitecoreUserPassword = "b",

    # The prefix for the Search index. Using the SiteName value for the prefix is recommended.
    [string]$SearchIndexPrefix = "1010",
    # The URL of the Solr Server.
    [string]$SolrUrl = "https://localhost:8987/solr",
    # The folder that Solr has been installed to.
    [string]$SolrRoot = "D:\Solr\solr-8.4.0",
    # The name of the Solr Service.
    [string]$SolrService = "solr-8.4.0",
    # The prefix for the Storefront index. The default value is the SiteNamePrefix.
    [string]$StorefrontIndexPrefix = $SiteNamePrefix,

    # The host name where Redis is hosted.
    [string]$RedisHost = "localhost",
    # The port number on which Redis is running.
    [string]$RedisPort = "6379",
    # The name of the Redis instance.
    [string]$RedisInstanceName = "Redis",
    # The path to the redis-cli executable.
    [string]$RedisCliPath = "$($Env:SYSTEMDRIVE)\Program Files\Redis\redis-cli.exe",

    # The location of the database server where Commerce databases should be deployed. In case of named SQL instance, use "SQLServerName\\SQLInstanceName"
    [string]$CommerceServicesDbServer = $($Env:COMPUTERNAME),
    # The name of the shared database for the Commerce Services.
    [string]$CommerceServicesDbName = "SitecoreCommerce1010_SharedEnvironments",
    # The name of the global database for the Commerce Services.
    [string]$CommerceServicesGlobalDbName = "SitecoreCommerce1010_Global",
    # The name of the archive database for the Commerce Services.
    [string]$CommerceServicesArchiveDbName = "SitecoreCommerce1010_ArchiveSharedEnvironments",
    # The port for the Commerce Ops Service.
    [string]$CommerceOpsServicesPort = "5015",
    # The port for the Commerce Shops Service
    [string]$CommerceShopsServicesPort = "5005",
    # The port for the Commerce Authoring Service.
    [string]$CommerceAuthoringServicesPort = "5000",
    # The port for the Commerce Minions Service.
    [string]$CommerceMinionsServicesPort = "5010",
    # The postfix appended to Commerce services folders names and sitenames.
    # The postfix allows you to host more than one Commerce installment on one server.
    [string]$CommerceServicesPostfix = "xc1010",
    # The postfix used as the root domain name (two-levels) to append as the hostname for Commerce services.
    # By default, all Commerce services are configured as sub-domains of the domain identified by the postfix.
    # Postfix validation enforces the following rules:
    # 1. The first level (TopDomainName) must be 2-7 characters in length and can contain alphabetical characters (a-z, A-Z) only. Numeric and special characters are not valid.
    # 2. The second level (DomainName) can contain alpha-numeric characters (a-z, A-Z,and 0-9) and can include one hyphen (-) character.
    # Special characters (wildcard (*)), for example, are not valid.
    [string]$CommerceServicesHostPostfix = "xc1010.com",

    # The name of the Sitecore XC Business Tools server.
    [string]$BizFxSiteName = "SitecoreBizFx1010",
    # The port of the Sitecore XC Business Tools server.
    [string]$BizFxPort = "4200",

    # The prefix used in the EnvironmentName setting in the config.json file for each Commerce Engine role.
    [string]$EnvironmentsPrefix = "Habitat",
    # The list of Commerce environment names. By default, the script deploys the AdventureWorks and the Habitat environments.
    [array]$Environments = @("AdventureWorksAuthoring", "HabitatAuthoring"),
    # Commerce environments GUIDs used to clean existing Redis cache during deployment. Default parameter values correspond to the default Commerce environment GUIDS.
    [array]$EnvironmentsGuids = @("78a1ea611f3742a7ac899a3f46d60ca5", "40e77b7b4be94186b53b5bfd89a6a83b"),
    # The environments running the minions service. (This is required, for example, for running indexing minions).
    [array]$MinionEnvironments = @("AdventureWorksMinions", "HabitatMinions"),
    # whether to deploy sample data for each environment.
    [bool]$DeploySampleData = $true,

    # The domain of the local account used for the various application pools created as part of the deployment.
    [string]$UserDomain = $Env:COMPUTERNAME,
    # The user name for a local account to be set up for the various application pools that are created as part of the deployment.
    [string]$UserName = "WindowsUserName",
    # The password for the $UserName.
    [string]$UserPassword = "WindowsUserPassword",

    # The Braintree Merchant Id.
    [string]$BraintreeMerchantId = "",
    # The Braintree Public Key.
    [string]$BraintreePublicKey = "",
    # The Braintree Private Key.
    [string]$BraintreePrivateKey = "",
    # The Braintree Environment.
    [string]$BraintreeEnvironment = "",

    # List of comma-separated task names to skip during Sitecore XC deployment.
    [string]$TasksToSkip = ""
)

Function Resolve-ItemPath {
    param (
        [Parameter(Mandatory = $true)]
        [ValidateNotNullorEmpty()]
        [string] $Path
    )
    process {
        if ([string]::IsNullOrWhiteSpace($Path)) {
            throw "Parameter could not be validated because it contains only whitespace. Please check script parameters."
        }
        $itemPath = Resolve-Path -Path $Path -ErrorAction SilentlyContinue | Select-Object -First 1
        if ([string]::IsNullOrEmpty($itemPath) -or (-not (Test-Path $itemPath))) {
            throw "Path [$Path] could not be resolved. Please check script parameters."
        }

        Write-Host "Found [$itemPath]."
        return $itemPath
    }
}

if (($SkipDeployStorefrontPackages -eq $true) -and ($SkipInstallDefaultStorefront -eq $false)) {
    throw "You cannot install the SXA Storefront without deploying necessary packages. If you want to install the SXA Storefront, set [SkipDeployStorefrontPackages] parameter to [false]."
}

if (($DeploySampleData -eq $false) -and ($SkipInstallDefaultStorefront -eq $false)) {
    throw "You cannot install the SXA Storefront without deploying sample data. If you want to install the SXA Storefront, set [DeploySampleData] parameter to [true]."
}

[string[]] $Skip = @()
if (-not ([string]::IsNullOrWhiteSpace($TasksToSkip))) {
    $TasksToSkip.Split(',') | ForEach-Object { $Skip += $_.Trim() }
}

Push-Location $PSScriptRoot

$modulesPath = ( Join-Path -Path $PWD -ChildPath "Modules" )
if ($env:PSModulePath -notlike "*$modulesPath*") {
    [Environment]::SetEnvironmentVariable("PSModulePath", "$env:PSModulePath;$modulesPath")
}

$deployCommerceParams = @{
    Path                                     = Resolve-ItemPath -Path $Path
    SolrSchemas                              = Resolve-ItemPath -Path $SolrSchemas
    SiteUtilitiesSrc                         = Resolve-ItemPath -Path $SiteUtilitiesSrc
    MergeToolFullPath                        = Resolve-ItemPath -Path $MergeToolFullPath
    AdventureWorksImagesWdpFullPath          = Resolve-ItemPath -Path $AdventureWorksImagesWdpFullPath
    CommerceConnectWdpFullPath               = Resolve-ItemPath -Path $CommerceConnectWdpFullPath
    CEConnectWdpFullPath                     = Resolve-ItemPath -Path $CEConnectWdpFullPath
    SXACommerceWdpFullPath                   = Resolve-ItemPath -Path $SXACommerceWdpFullPath
    SXAStorefrontCatalogWdpFullPath          = Resolve-ItemPath -Path $SXAStorefrontCatalogWdpFullPath
    SXAStorefrontWdpFullPath                 = Resolve-ItemPath -Path $SXAStorefrontWdpFullPath
    SXAStorefrontThemeWdpFullPath            = Resolve-ItemPath -Path $SXAStorefrontThemeWdpFullPath
    CommercexAnalyticsWdpFullPath            = Resolve-ItemPath -Path $CommercexAnalyticsWdpFullPath
    CommercexProfilesWdpFullPath             = Resolve-ItemPath -Path $CommercexProfilesWdpFullPath
    CommerceMAWdpFullPath                    = Resolve-ItemPath -Path $CommerceMAWdpFullPath
    CommerceMAForAutomationEngineZIPFullPath = Resolve-ItemPath -Path $CommerceMAForAutomationEngineZIPFullPath
    SXAModuleZIPFullPath                     = Resolve-ItemPath -Path $SXAModuleZIPFullPath
    PowerShellExtensionsModuleZIPFullPath    = Resolve-ItemPath -Path $PowerShellExtensionsModuleZIPFullPath
    BizFxPackage                             = Resolve-ItemPath -Path $BizFxPackage
    CommerceEngineWdpFullPath                = Resolve-ItemPath -Path $CommerceEngineWdpFullPath
    HabitatImagesWdpFullPath                 = Resolve-ItemPath -Path $HabitatImagesWdpFullPath
    SiteName                                 = $SiteName
    MAEnginePrefix                           = $MAEnginePrefix
    SiteHostHeaderName                       = $SiteHostHeaderName
    InstallDir                               = Resolve-ItemPath -Path $InstallDir
    XConnectInstallDir                       = Resolve-ItemPath -Path $XConnectInstallDir
    CommerceInstallRoot                      = Resolve-ItemPath -Path $CommerceInstallRoot
    CommerceServicesDbServer                 = $CommerceServicesDbServer
    CommerceServicesDbName                   = $CommerceServicesDbName
    CommerceServicesGlobalDbName             = $CommerceServicesGlobalDbName
    CommerceServicesArchiveDbName            = $CommerceServicesArchiveDbName
    SitecoreDbServer                         = $SitecoreDbServer
    SitecoreCoreDbName                       = $SitecoreCoreDbName
    SqlDbPrefix                              = $SqlDbPrefix
    SqlAdminUser                             = $SqlUser
    SqlAdminPassword                         = $SqlPass
    SolrUrl                                  = $SolrUrl
    SolrRoot                                 = Resolve-ItemPath -Path $SolrRoot
    SolrService                              = $SolrService
    SearchIndexPrefix                        = $SearchIndexPrefix
    StorefrontIndexPrefix                    = $StorefrontIndexPrefix
    CommerceServicesPostfix                  = $CommerceServicesPostfix
    CommerceServicesHostPostfix              = $CommerceServicesHostPostfix
    EnvironmentsPrefix                       = $EnvironmentsPrefix
    Environments                             = $Environments
    EnvironmentsGuids                        = $EnvironmentsGuids
    MinionEnvironments                       = $MinionEnvironments
    CommerceOpsServicesPort                  = $CommerceOpsServicesPort
    CommerceShopsServicesPort                = $CommerceShopsServicesPort
    CommerceAuthoringServicesPort            = $CommerceAuthoringServicesPort
    CommerceMinionsServicesPort              = $CommerceMinionsServicesPort
    RedisInstanceName                        = $RedisInstanceName
    RedisCliPath                             = $RedisCliPath
    RedisHost                                = $RedisHost
    RedisPort                                = $RedisPort
    UserDomain                               = $UserDomain
    UserName                                 = $UserName
    UserPassword                             = $UserPassword
    BraintreeMerchantId                      = $BraintreeMerchantId
    BraintreePublicKey                       = $BraintreePublicKey
    BraintreePrivateKey                      = $BraintreePrivateKey
    BraintreeEnvironment                     = $BraintreeEnvironment
    SitecoreDomain                           = $SitecoreDomain
    SitecoreUsername                         = $SitecoreUsername
    SitecoreUserPassword                     = $SitecoreUserPassword
    BizFxSiteName                            = $BizFxSiteName
    BizFxPort                                = $BizFxPort
    SitecoreIdentityServerApplicationName    = $IdentityServerSiteName
    SitecoreIdentityServerUrl                = $SitecoreIdentityServerUrl
    SkipInstallDefaultStorefront             = $SkipInstallDefaultStorefront
    SkipDeployStorefrontPackages             = $SkipDeployStorefrontPackages
    CommerceEngineConnectClientId            = $CommerceEngineConnectClientId
    CommerceEngineConnectClientSecret        = $CommerceEngineConnectClientSecret
    DeploySampleData                         = $DeploySampleData
}

if ($Skip.Count -eq 0) {
    Install-SitecoreConfiguration @deployCommerceParams -Verbose *>&1 | Tee-Object "$XCSIFInstallRoot\XC-Install.log"

    # Uncomment the below line and comment out the above if you want to remove the Sitecore XC
    #Uninstall-SitecoreConfiguration @deployCommerceParams -Verbose *>&1 | Tee-Object "$XCSIFInstallRoot\XC-Uninstall.log"
}
else {
    Install-SitecoreConfiguration @deployCommerceParams -Skip $Skip -Verbose *>&1 | Tee-Object "$XCSIFInstallRoot\XC-Install.log"
}

# SIG # Begin signature block
# MIIbVQYJKoZIhvcNAQcCoIIbRjCCG0ICAQExCzAJBgUrDgMCGgUAMGkGCisGAQQB
# gjcCAQSgWzBZMDQGCisGAQQBgjcCAR4wJgIDAQAABBAfzDtgWUsITrck0sYpfvNR
# AgEAAgEAAgEAAgEAAgEAMCEwCQYFKw4DAhoFAAQU/E06Wtjw8h4/2LwWhNO6Bkn6
# aw2gggpvMIIFMDCCBBigAwIBAgIQBAkYG1/Vu2Z1U0O1b5VQCDANBgkqhkiG9w0B
# AQsFADBlMQswCQYDVQQGEwJVUzEVMBMGA1UEChMMRGlnaUNlcnQgSW5jMRkwFwYD
# VQQLExB3d3cuZGlnaWNlcnQuY29tMSQwIgYDVQQDExtEaWdpQ2VydCBBc3N1cmVk
# IElEIFJvb3QgQ0EwHhcNMTMxMDIyMTIwMDAwWhcNMjgxMDIyMTIwMDAwWjByMQsw
# CQYDVQQGEwJVUzEVMBMGA1UEChMMRGlnaUNlcnQgSW5jMRkwFwYDVQQLExB3d3cu
# ZGlnaWNlcnQuY29tMTEwLwYDVQQDEyhEaWdpQ2VydCBTSEEyIEFzc3VyZWQgSUQg
# Q29kZSBTaWduaW5nIENBMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA
# +NOzHH8OEa9ndwfTCzFJGc/Q+0WZsTrbRPV/5aid2zLXcep2nQUut4/6kkPApfmJ
# 1DcZ17aq8JyGpdglrA55KDp+6dFn08b7KSfH03sjlOSRI5aQd4L5oYQjZhJUM1B0
# sSgmuyRpwsJS8hRniolF1C2ho+mILCCVrhxKhwjfDPXiTWAYvqrEsq5wMWYzcT6s
# cKKrzn/pfMuSoeU7MRzP6vIK5Fe7SrXpdOYr/mzLfnQ5Ng2Q7+S1TqSp6moKq4Tz
# rGdOtcT3jNEgJSPrCGQ+UpbB8g8S9MWOD8Gi6CxR93O8vYWxYoNzQYIH5DiLanMg
# 0A9kczyen6Yzqf0Z3yWT0QIDAQABo4IBzTCCAckwEgYDVR0TAQH/BAgwBgEB/wIB
# ADAOBgNVHQ8BAf8EBAMCAYYwEwYDVR0lBAwwCgYIKwYBBQUHAwMweQYIKwYBBQUH
# AQEEbTBrMCQGCCsGAQUFBzABhhhodHRwOi8vb2NzcC5kaWdpY2VydC5jb20wQwYI
# KwYBBQUHMAKGN2h0dHA6Ly9jYWNlcnRzLmRpZ2ljZXJ0LmNvbS9EaWdpQ2VydEFz
# c3VyZWRJRFJvb3RDQS5jcnQwgYEGA1UdHwR6MHgwOqA4oDaGNGh0dHA6Ly9jcmw0
# LmRpZ2ljZXJ0LmNvbS9EaWdpQ2VydEFzc3VyZWRJRFJvb3RDQS5jcmwwOqA4oDaG
# NGh0dHA6Ly9jcmwzLmRpZ2ljZXJ0LmNvbS9EaWdpQ2VydEFzc3VyZWRJRFJvb3RD
# QS5jcmwwTwYDVR0gBEgwRjA4BgpghkgBhv1sAAIEMCowKAYIKwYBBQUHAgEWHGh0
# dHBzOi8vd3d3LmRpZ2ljZXJ0LmNvbS9DUFMwCgYIYIZIAYb9bAMwHQYDVR0OBBYE
# FFrEuXsqCqOl6nEDwGD5LfZldQ5YMB8GA1UdIwQYMBaAFEXroq/0ksuCMS1Ri6en
# IZ3zbcgPMA0GCSqGSIb3DQEBCwUAA4IBAQA+7A1aJLPzItEVyCx8JSl2qB1dHC06
# GsTvMGHXfgtg/cM9D8Svi/3vKt8gVTew4fbRknUPUbRupY5a4l4kgU4QpO4/cY5j
# DhNLrddfRHnzNhQGivecRk5c/5CxGwcOkRX7uq+1UcKNJK4kxscnKqEpKBo6cSgC
# PC6Ro8AlEeKcFEehemhor5unXCBc2XGxDI+7qPjFEmifz0DLQESlE/DmZAwlCEIy
# sjaKJAL+L3J+HNdJRZboWR3p+nRka7LrZkPas7CM1ekN3fYBIM6ZMWM9CBoYs4Gb
# T8aTEAb8B4H6i9r5gkn3Ym6hU/oSlBiFLpKR6mhsRDKyZqHnGKSaZFHvMIIFNzCC
# BB+gAwIBAgIQD7DIiKzIDvOVbd7QfMY3fjANBgkqhkiG9w0BAQsFADByMQswCQYD
# VQQGEwJVUzEVMBMGA1UEChMMRGlnaUNlcnQgSW5jMRkwFwYDVQQLExB3d3cuZGln
# aWNlcnQuY29tMTEwLwYDVQQDEyhEaWdpQ2VydCBTSEEyIEFzc3VyZWQgSUQgQ29k
# ZSBTaWduaW5nIENBMB4XDTIwMDkwMzAwMDAwMFoXDTIxMTEwMTEyMDAwMFowdDEL
# MAkGA1UEBhMCVVMxEzARBgNVBAgTCkNhbGlmb3JuaWExFjAUBgNVBAcTDVNhbiBG
# cmFuY2lzY28xGzAZBgNVBAoTElNpdGVjb3JlIFVTQSwgSW5jLjEbMBkGA1UEAxMS
# U2l0ZWNvcmUgVVNBLCBJbmMuMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKC
# AQEAzVvUn7sL32KesFMdld4mE20SLgZeDQNgd72nw4uPnNfKnSAMSctBSRapyZ8n
# rApWObVxfr5wrRqS93IXP1Hmf5wNxOIzV69mKtDKVLuLGVIVfY54qBFNa95tbyJX
# fZUU38BVgv1vJoV+XMvbCueF2aBACeBiCQ2CBoGujr/L5LlTteOH81UXM23yWJt2
# 5gNIoi5zscnn0IC10jvUcuw6YNcmMZYR8V2BJKdqL2T4NsA16aG+vDk13wdhxqfw
# CwYTWiTR8iIszZl5/ZM2dYn9DJNTPP1RhYEGlwdv6ppjr6fQqWC1NSYi62nc9IzB
# Z7k7zkiZ2ll8MamvrCnwQThFcQIDAQABo4IBxTCCAcEwHwYDVR0jBBgwFoAUWsS5
# eyoKo6XqcQPAYPkt9mV1DlgwHQYDVR0OBBYEFD3QmHkA+pK+PANv4T8P0Xc5tBbY
# MA4GA1UdDwEB/wQEAwIHgDATBgNVHSUEDDAKBggrBgEFBQcDAzB3BgNVHR8EcDBu
# MDWgM6Axhi9odHRwOi8vY3JsMy5kaWdpY2VydC5jb20vc2hhMi1hc3N1cmVkLWNz
# LWcxLmNybDA1oDOgMYYvaHR0cDovL2NybDQuZGlnaWNlcnQuY29tL3NoYTItYXNz
# dXJlZC1jcy1nMS5jcmwwTAYDVR0gBEUwQzA3BglghkgBhv1sAwEwKjAoBggrBgEF
# BQcCARYcaHR0cHM6Ly93d3cuZGlnaWNlcnQuY29tL0NQUzAIBgZngQwBBAEwgYQG
# CCsGAQUFBwEBBHgwdjAkBggrBgEFBQcwAYYYaHR0cDovL29jc3AuZGlnaWNlcnQu
# Y29tME4GCCsGAQUFBzAChkJodHRwOi8vY2FjZXJ0cy5kaWdpY2VydC5jb20vRGln
# aUNlcnRTSEEyQXNzdXJlZElEQ29kZVNpZ25pbmdDQS5jcnQwDAYDVR0TAQH/BAIw
# ADANBgkqhkiG9w0BAQsFAAOCAQEA8ZhbyfAXwHnmgE3ghKSf8DpKpaPncViCv1j+
# gaiBqOpdGhptU+ag18WFLKui9FRmhjrZ0qp08eNftfoITW53FbBe0o2GgnapsRKg
# tXpm/25/zh4h/AHGMA1A2n4fZNFrVGEIdokCHIuabyaYKEGN/r0iVs8ZZYktQIAW
# F0QE1spdf/AoP4xGcgoZgRgdEc4smMj6OE83kca00HVEoli7mC/eBSo/iUyDZYo0
# ANFST3GCQ/URgpQz0kFJfAjrLdxTC4I+rHm0i8XdtQOpoi8N25CfJR9560q9OnjH
# lAYmmXg0y7W3uEdFvMrg3yfYCJFDisWK2rKEn7YP+Q7ujjWCjjGCEFAwghBMAgEB
# MIGGMHIxCzAJBgNVBAYTAlVTMRUwEwYDVQQKEwxEaWdpQ2VydCBJbmMxGTAXBgNV
# BAsTEHd3dy5kaWdpY2VydC5jb20xMTAvBgNVBAMTKERpZ2lDZXJ0IFNIQTIgQXNz
# dXJlZCBJRCBDb2RlIFNpZ25pbmcgQ0ECEA+wyIisyA7zlW3e0HzGN34wCQYFKw4D
# AhoFAKBwMBAGCisGAQQBgjcCAQwxAjAAMBkGCSqGSIb3DQEJAzEMBgorBgEEAYI3
# AgEEMBwGCisGAQQBgjcCAQsxDjAMBgorBgEEAYI3AgEVMCMGCSqGSIb3DQEJBDEW
# BBRwP8qe+TCSNKLHHZ4a+D1S8EN0ujANBgkqhkiG9w0BAQEFAASCAQBMA9mgNgQ7
# Kl/k8MAXU7QwFtM27tUR3DXuUxEvF1hUoA0p2pYpbgYqoMoW1+nTW9l61dnwfi/8
# jGV5JHOoaPWmIcck+yf6YmVHYsBKS9VdEThJU68ekRVpap5KrZZWSoSXj0oVk9lP
# aw3pitAt12loJ4ovkDgta+bo0G9hS98l8wZJO0/pjQUxkTinTgI3A7L3RJZ4rEbF
# lluFbziSW6fvBgnlaeTMAoMvwaoGyuZHsDXGe5xKTAPQDsXvskVvgeyM5ciJ5UOn
# iYpRWHsl1sQ+kJfARG2IGomNnF/frmElnRWq9T/0JWMQbtFTlm+QseIIoNRMKOks
# ktgmYJHc8TUuoYIOLDCCDigGCisGAQQBgjcDAwExgg4YMIIOFAYJKoZIhvcNAQcC
# oIIOBTCCDgECAQMxDTALBglghkgBZQMEAgEwgf8GCyqGSIb3DQEJEAEEoIHvBIHs
# MIHpAgEBBgtghkgBhvhFAQcXAzAhMAkGBSsOAwIaBQAEFDQvoFWpqy9dm9sX43gj
# fZ2YReSHAhUA0Hn82PKHhHrXAu5XBOCNK+nXhxAYDzIwMjEwMjE4MTg1NjAwWjAD
# AgEeoIGGpIGDMIGAMQswCQYDVQQGEwJVUzEdMBsGA1UEChMUU3ltYW50ZWMgQ29y
# cG9yYXRpb24xHzAdBgNVBAsTFlN5bWFudGVjIFRydXN0IE5ldHdvcmsxMTAvBgNV
# BAMTKFN5bWFudGVjIFNIQTI1NiBUaW1lU3RhbXBpbmcgU2lnbmVyIC0gRzOgggqL
# MIIFODCCBCCgAwIBAgIQewWx1EloUUT3yYnSnBmdEjANBgkqhkiG9w0BAQsFADCB
# vTELMAkGA1UEBhMCVVMxFzAVBgNVBAoTDlZlcmlTaWduLCBJbmMuMR8wHQYDVQQL
# ExZWZXJpU2lnbiBUcnVzdCBOZXR3b3JrMTowOAYDVQQLEzEoYykgMjAwOCBWZXJp
# U2lnbiwgSW5jLiAtIEZvciBhdXRob3JpemVkIHVzZSBvbmx5MTgwNgYDVQQDEy9W
# ZXJpU2lnbiBVbml2ZXJzYWwgUm9vdCBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTAe
# Fw0xNjAxMTIwMDAwMDBaFw0zMTAxMTEyMzU5NTlaMHcxCzAJBgNVBAYTAlVTMR0w
# GwYDVQQKExRTeW1hbnRlYyBDb3Jwb3JhdGlvbjEfMB0GA1UECxMWU3ltYW50ZWMg
# VHJ1c3QgTmV0d29yazEoMCYGA1UEAxMfU3ltYW50ZWMgU0hBMjU2IFRpbWVTdGFt
# cGluZyBDQTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALtZnVlVT52M
# cl0agaLrVfOwAa08cawyjwVrhponADKXak3JZBRLKbvC2Sm5Luxjs+HPPwtWkPhi
# G37rpgfi3n9ebUA41JEG50F8eRzLy60bv9iVkfPw7mz4rZY5Ln/BJ7h4OcWEpe3t
# r4eOzo3HberSmLU6Hx45ncP0mqj0hOHE0XxxxgYptD/kgw0mw3sIPk35CrczSf/K
# O9T1sptL4YiZGvXA6TMU1t/HgNuR7v68kldyd/TNqMz+CfWTN76ViGrF3PSxS9TO
# 6AmRX7WEeTWKeKwZMo8jwTJBG1kOqT6xzPnWK++32OTVHW0ROpL2k8mc40juu1MO
# 1DaXhnjFoTcCAwEAAaOCAXcwggFzMA4GA1UdDwEB/wQEAwIBBjASBgNVHRMBAf8E
# CDAGAQH/AgEAMGYGA1UdIARfMF0wWwYLYIZIAYb4RQEHFwMwTDAjBggrBgEFBQcC
# ARYXaHR0cHM6Ly9kLnN5bWNiLmNvbS9jcHMwJQYIKwYBBQUHAgIwGRoXaHR0cHM6
# Ly9kLnN5bWNiLmNvbS9ycGEwLgYIKwYBBQUHAQEEIjAgMB4GCCsGAQUFBzABhhJo
# dHRwOi8vcy5zeW1jZC5jb20wNgYDVR0fBC8wLTAroCmgJ4YlaHR0cDovL3Muc3lt
# Y2IuY29tL3VuaXZlcnNhbC1yb290LmNybDATBgNVHSUEDDAKBggrBgEFBQcDCDAo
# BgNVHREEITAfpB0wGzEZMBcGA1UEAxMQVGltZVN0YW1wLTIwNDgtMzAdBgNVHQ4E
# FgQUr2PWyqNOhXLgp7xB8ymiOH+AdWIwHwYDVR0jBBgwFoAUtnf6aUhHn1MS1cLq
# BzJ2B9GXBxkwDQYJKoZIhvcNAQELBQADggEBAHXqsC3VNBlcMkX+DuHUT6Z4wW/X
# 6t3cT/OhyIGI96ePFeZAKa3mXfSi2VZkhHEwKt0eYRdmIFYGmBmNXXHy+Je8Cf0c
# kUfJ4uiNA/vMkC/WCmxOM+zWtJPITJBjSDlAIcTd1m6JmDy1mJfoqQa3CcmPU1dB
# kC/hHk1O3MoQeGxCbvC2xfhhXFL1TvZrjfdKer7zzf0D19n2A6gP41P3CnXsxnUu
# qmaFBJm3+AZX4cYO9uiv2uybGB+queM6AL/OipTLAduexzi7D1Kr0eOUA2AKTaD+
# J20UMvw/l0Dhv5mJ2+Q5FL3a5NPD6itas5VYVQR9x5rsIwONhSrS/66pYYEwggVL
# MIIEM6ADAgECAhB71OWvuswHP6EBIwQiQU0SMA0GCSqGSIb3DQEBCwUAMHcxCzAJ
# BgNVBAYTAlVTMR0wGwYDVQQKExRTeW1hbnRlYyBDb3Jwb3JhdGlvbjEfMB0GA1UE
# CxMWU3ltYW50ZWMgVHJ1c3QgTmV0d29yazEoMCYGA1UEAxMfU3ltYW50ZWMgU0hB
# MjU2IFRpbWVTdGFtcGluZyBDQTAeFw0xNzEyMjMwMDAwMDBaFw0yOTAzMjIyMzU5
# NTlaMIGAMQswCQYDVQQGEwJVUzEdMBsGA1UEChMUU3ltYW50ZWMgQ29ycG9yYXRp
# b24xHzAdBgNVBAsTFlN5bWFudGVjIFRydXN0IE5ldHdvcmsxMTAvBgNVBAMTKFN5
# bWFudGVjIFNIQTI1NiBUaW1lU3RhbXBpbmcgU2lnbmVyIC0gRzMwggEiMA0GCSqG
# SIb3DQEBAQUAA4IBDwAwggEKAoIBAQCvDoqq+Ny/aXtUF3FHCb2NPIH4dBV3Z5Cc
# /d5OAp5LdvblNj5l1SQgbTD53R2D6T8nSjNObRaK5I1AjSKqvqcLG9IHtjy1GiQo
# +BtyUT3ICYgmCDr5+kMjdUdwDLNfW48IHXJIV2VNrwI8QPf03TI4kz/lLKbzWSPL
# gN4TTfkQyaoKGGxVYVfR8QIsxLWr8mwj0p8NDxlsrYViaf1OhcGKUjGrW9jJdFLj
# V2wiv1V/b8oGqz9KtyJ2ZezsNvKWlYEmLP27mKoBONOvJUCbCVPwKVeFWF7qhUhB
# IYfl3rTTJrJ7QFNYeY5SMQZNlANFxM48A+y3API6IsW0b+XvsIqbAgMBAAGjggHH
# MIIBwzAMBgNVHRMBAf8EAjAAMGYGA1UdIARfMF0wWwYLYIZIAYb4RQEHFwMwTDAj
# BggrBgEFBQcCARYXaHR0cHM6Ly9kLnN5bWNiLmNvbS9jcHMwJQYIKwYBBQUHAgIw
# GRoXaHR0cHM6Ly9kLnN5bWNiLmNvbS9ycGEwQAYDVR0fBDkwNzA1oDOgMYYvaHR0
# cDovL3RzLWNybC53cy5zeW1hbnRlYy5jb20vc2hhMjU2LXRzcy1jYS5jcmwwFgYD
# VR0lAQH/BAwwCgYIKwYBBQUHAwgwDgYDVR0PAQH/BAQDAgeAMHcGCCsGAQUFBwEB
# BGswaTAqBggrBgEFBQcwAYYeaHR0cDovL3RzLW9jc3Aud3Muc3ltYW50ZWMuY29t
# MDsGCCsGAQUFBzAChi9odHRwOi8vdHMtYWlhLndzLnN5bWFudGVjLmNvbS9zaGEy
# NTYtdHNzLWNhLmNlcjAoBgNVHREEITAfpB0wGzEZMBcGA1UEAxMQVGltZVN0YW1w
# LTIwNDgtNjAdBgNVHQ4EFgQUpRMBqZ+FzBtuFh5fOzGqeTYAex0wHwYDVR0jBBgw
# FoAUr2PWyqNOhXLgp7xB8ymiOH+AdWIwDQYJKoZIhvcNAQELBQADggEBAEaer/C4
# ol+imUjPqCdLIc2yuaZycGMv41UpezlGTud+ZQZYi7xXipINCNgQujYk+gp7+zvT
# Yr9KlBXmgtuKVG3/KP5nz3E/5jMJ2aJZEPQeSv5lzN7Ua+NSKXUASiulzMub6KlN
# 97QXWZJBw7c/hub2wH9EPEZcF1rjpDvVaSbVIX3hgGd+Yqy3Ti4VmuWcI69bEepx
# qUH5DXk4qaENz7Sx2j6aescixXTN30cJhsT8kSWyG5bphQjo3ep0YG5gpVZ6DchE
# WNzm+UgUnuW/3gC9d7GYFHIUJN/HESwfAD/DSxTGZxzMHgajkF9cVIs+4zNbgg/F
# t4YCTnGf6WZFP3YxggJaMIICVgIBATCBizB3MQswCQYDVQQGEwJVUzEdMBsGA1UE
# ChMUU3ltYW50ZWMgQ29ycG9yYXRpb24xHzAdBgNVBAsTFlN5bWFudGVjIFRydXN0
# IE5ldHdvcmsxKDAmBgNVBAMTH1N5bWFudGVjIFNIQTI1NiBUaW1lU3RhbXBpbmcg
# Q0ECEHvU5a+6zAc/oQEjBCJBTRIwCwYJYIZIAWUDBAIBoIGkMBoGCSqGSIb3DQEJ
# AzENBgsqhkiG9w0BCRABBDAcBgkqhkiG9w0BCQUxDxcNMjEwMjE4MTg1NjAwWjAv
# BgkqhkiG9w0BCQQxIgQgc4qb+py2M6+3xdCmUTgpkGLJ4NHu5C8SJ9TmOme0KWYw
# NwYLKoZIhvcNAQkQAi8xKDAmMCQwIgQgxHTOdgB9AjlODaXk3nwUxoD54oIBPP72
# U+9dtx/fYfgwCwYJKoZIhvcNAQEBBIIBAAv8C0KfEzIj/glC+83HU0LCtqrBydw3
# 33escBif/M+Hzz3TDYcLVUT/mih4k0xsmPlah6PkBSEN8gHDocyzPfWX7kEIi3VR
# hYYtcZotLKLv4DhYqs9IyraZF6kz0v6Bkawwe8uSddVA1PYOYlAPdfiJSvoo6dJT
# HJi1jeeAG20u1bR6htdt6Xt8H9bJLdLeIl1YRqCWaOhrIB/49G/i9vFdZfeAoZBv
# 7F/vw8I3T95HG/MrCOjYBdUvpdVtAJkki+D0aFYjXLqR/g1DosEYmNtsIXNGBqCF
# XTgUSk/2mZBxMDLnVMifgu/pzhph5L7rv8JsTgtKUag/On7+Nc9Y708=
# SIG # End signature block
