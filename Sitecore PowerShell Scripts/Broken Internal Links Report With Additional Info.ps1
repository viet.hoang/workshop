<#
    .SYNOPSIS
        Lists the items with broken internal links searching all or the latest version in the current language.
        
    .NOTES
        Adam Najmanowicz, Michael West, Daniel Scherrer
        Adapted from the Advanced System Reporter module & Daniel Scherrer's external links checker: 
        https://gist.github.com/daniiiol/143db3e2004afe9a55c1dd3e33048940
		
		Viet Hoang clones /sitecore/system/Modules/PowerShell/Script Library/Content Reports/Reports/Content Audit/Broken Links and updates it for his need
#>

$database = "master"
$root = Get-Item -Path (@{$true="$($database):\content"; $false="$($database):\content"}[(Test-Path -Path "$($database):\content\home")])

$versionOptions = [ordered]@{
    "Latest"="1"
}

$props = @{
    Parameters = @(
        @{Name="root"; Title="Choose the report root"; Tooltip="Only items in this branch will be returned."; Columns=9},
        @{Name="searchVersion"; Value="1"; Title="Version"; Options=$versionOptions; Tooltip="Choose a version."; Columns="3"; Placeholder="All"}
    )
    Title = "Broken Internal Links Report With Additional Info"
    Description = "Choose the criteria for the report."
    Width = 550
    Height = 300
    ShowHints = $true
}

$result = Read-Variable @props

if($result -eq "cancel"){
    exit
}

filter HasBrokenLink {
    param(
        [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
        [Sitecore.Data.Items.Item]$Item,
        
        [Parameter()]
        [bool]$IncludeAllVersions
    )
    
    if($Item) {
        try {
            $brokenLinks = $item.Links.GetBrokenLinks($IncludeAllVersions)
            if($brokenlinks -ne $null -and $brokenlinks.Length -gt 0) {
                $num = 0
                foreach($brokenLink in $brokenLinks) {
                    $num = $num + 1
    				$brokenRenderingDefinition = Get-Rendering -Item $item -Datasource $brokenLink.TargetPath
    				if ($brokenRenderingDefinition) {
    				    $statusCode = "Missing Target Item"
    				    $brokenRendering = Get-Item . -Database ($root.Database) -ID $brokenRenderingDefinition.ItemID
    				    $brokenAdditionalInfo = "Placeholder: " + $brokenRenderingDefinition.Placeholder + " (" +  $brokenRendering.ItemPath + ")"
    				}
    				else {
    				    $sourceFieldName = (Get-Item . -Database ($root.Database) -ID $brokenLink.SourceFieldID).Name
    				    $statusCode = "Unknown " + $sourceFieldName
    				    $brokenAdditionalInfo = "Unknown " + $sourceFieldName + ": " + $brokenLink.TargetPath
    				}
    				
                    $brokenItem = $brokenLink.GetSourceItem() | Initialize-Item
                    
                    if (!$brokenItem."Broken Link Field") {
                        Add-Member -InputObject $brokenItem -NotePropertyName "Broken Link Field" -NotePropertyValue ($num.ToString() + ") " + ((Get-Item . -Database ($root.Database) -ID $brokenLink.SourceFieldID).Name))
                    }
                    else {
                        $brokenItem."Broken Link Field" = $brokenItem."Broken Link Field" + "<br />" + $num.ToString() + ") " + ((Get-Item . -Database ($root.Database) -ID $brokenLink.SourceFieldID).Name)
                    }
                    
                    if (!$brokenItem."Target Url") {
                        Add-Member -InputObject $brokenItem -NotePropertyName "Target Url" -NotePropertyValue ($num.ToString() + ") " + $brokenLink.TargetPath)
                    }
                    else {
                        $brokenItem."Target Url" = $brokenItem."Target Url" + "<br />" + $num.ToString() + ") " + $brokenLink.TargetPath
                    }
                    
                    if (!$brokenItem."Status Code") {
                        Add-Member -InputObject $brokenItem -NotePropertyName "Status Code" -NotePropertyValue ($num.ToString() + ") " + $statusCode)
                    }
                    else {
                        $brokenItem."Status Code" = $brokenItem."Status Code" + "<br />" + $num.ToString() + ") " + $statusCode
                    }
                    
                    if (!$brokenItem."Additional Info") {
        			    Add-Member -InputObject $brokenItem -NotePropertyName "Additional Info" -NotePropertyValue ($num.ToString() + ") " + $brokenAdditionalInfo)
        			    $brokenItem
                    }
                    else {
                        $brokenItem."Additional Info" = $brokenItem."Additional Info" + "<br />" + $num.ToString() + ") " + $brokenAdditionalInfo
                    }
                }
            }
        }
        catch {
            Write-Warning $Error[0]
            Write-Host "Problematic item ID = " $Item.Id
        }
    }
}

$items = Get-ChildItem -Path $root.ProviderPath -Recurse | HasBrokenLink -IncludeAllVersions (!$searchVersion)

if($items.Count -eq 0){
    Show-Alert "There are no items found which have broken internal links in the current language."
} else {
    $props = @{
        Title = "Broken Internal Links Report With Additional Info"
        InfoTitle = "$($items.Count) items with broken internal links found!"
        InfoDescription = "The report checked for internal links in $(@('all versions','latest versions')[[byte]($searchVersion='1')]) of items."
        MissingDataMessage = "There are no items found which have broken internal links in the current language."
        PageSize = 25
    }
    
    $items |
        Show-ListView @props -Property "Status Code", "Broken Link Field","Target Url", "Additional Info",
            @{Label="Name"; Expression={$_.DisplayName} },
            @{Label="Path"; Expression={$_.ItemPath} },
            "Version",
            "Language",
            @{Label="Updated"; Expression={$_.__Updated} },
            @{Label="Updated by"; Expression={$_."__Updated by"} }
			
}
Close-Window