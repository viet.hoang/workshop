$targetPath = "%vhs.unicorn.sourceFolder%\seria"
$sourcePath = "%system.teamcity.build.checkoutDir%\seria"
$FoldersExclude = '"code" "roles" "Tests"'
$FilesExclude = ""

# /e: Copies subdirectories. Note that this option includes empty directories
# With the /e plus /purge options, if the destination directory exists, the destination directory security settings are not overwritten.
# /xd Excludes directories that match the specified names and paths.
# /xf Excludes files that match the specified names or paths. Note that FileName can include wildcard characters (* and ?).
robocopy $sourcePath $targetPath /e /purge /xd $FoldersExclude /xf $FilesExclude