$folder = ".\src\Foundation\VietHoang.Sc\App_Config\Include\VietHoang\Foundation\"
$fileName = "VietHoang.Sc.DevSettings.config"
$filePath = $folder + $fileName

if (Test-Path $filePath)
{
 remove-Item $filePath
}

$content = '<?xml version="1.0"?>
<configuration xmlns:patch="http://www.sitecore.net/xmlconfig/">
  <sitecore>
	<sc.variable name="dataFolder" value="%vhs.instance.root%\%vhs.instance.name_CM%.data" />
    <sc.variable name="VietHoangSourceFolder" value="{0}" />
    <settings>
      <!--  Rainbow - SERIALIZATION FOLDER PATH MAX LENGTH
		  In Windows, there is 248 characters limit on the lenght of file system paths. To avoid exceeding the maximum path length, Rainbow will loop
				  long paths back to the root. This setting specifies the maximum length of the path to the serialization root path,
				  which determines how long item paths can be before they are looped.
		  Important: The value of this setting must be the same on all Sitecore instances accessing the serialized data. 
		  Important: When changing this value, you must reserialize all configurations!
		  Example: A value of "90" for this setting will mean that item paths longer than 150 characters will be shortened, since Sitecore 
		  reserves 8 characters (and 248 - 8 - 90 = 150). 
		  Default value: 90
		-->
      <setting name="Rainbow.SFS.SerializationFolderPathMaxLength" value="150" />
      <setting name="Rainbow.SFS.MaxItemNameLengthBeforeTruncation" value="50" />
    </settings>
  </sitecore>
</configuration>
' -f "%vhs.unicorn.sourceFolder%"

new-item -path $folder -name $fileName -type "file" -value $content

Get-Content $filePath