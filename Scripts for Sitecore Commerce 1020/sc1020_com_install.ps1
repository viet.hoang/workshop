#Requires -Version 3
param(
	# The root folder of Sitecore instances
	[string]$ScInstallDirectory = "C:\websites",
    # The root folder with WDP files.
    [string]$XCInstallRoot = "..",
    # The root folder of SIF.Sitecore.Commerce package.
    [string]$XCSIFInstallRoot = $PWD,
    # Specifies whether or not to bypass the installation of the default SXA Storefront. By default, the Sitecore XC installation script also deploys the SXA Storefront.
    [bool]$SkipInstallDefaultStorefront = $false,
    # Specifies whether or not to bypass the installation of the SXA Storefront packages.
    # If set to $true, $TasksToSkip parameter will be populated with the list of tasks to skip in order to bypass SXA Storefront packages installation.
    [bool]$SkipDeployStorefrontPackages = $false,

    # Path to the Master_SingleServer.json file provided in the SIF.Sitecore.Commerce package.
    [string]$Path = "$XCSIFInstallRoot\Configuration\Commerce\Master_SingleServer.json",
    # Path to the Commerce Solr schemas provided as part of the SIF.Sitecore.Commerce package.
    [string]$SolrSchemas = "$XCSIFInstallRoot\SolrSchemas",
    # Path to the SiteUtilityPages folder provided as part of the SIF.Sitecore.Commerce package.
    [string]$SiteUtilitiesSrc = "$XCSIFInstallRoot\SiteUtilityPages",
    # Path to the location where you downloaded the Microsoft.Web.XmlTransform.dll file.
    [string]$MergeToolFullPath = "$XCInstallRoot\Microsoft.Web.XmlTransform.dll",
    # Path to the Adventure Works Images.OnPrem SCWDP file
    [string]$AdventureWorksImagesWdpFullPath = "$XCInstallRoot\Adventure Works Images.OnPrem.scwdp.zip",
    # Path to the Sitecore Commerce Connect Core SCWDP file.
    [string]$CommerceConnectWdpFullPath = "$XCInstallRoot\Sitecore Commerce Connect Core*.scwdp.zip",
    # Path to the Sitecore Commerce Engine Connect OnPrem SCWDP file.
    [string]$CEConnectWdpFullPath = "$XCInstallRoot\Sitecore Commerce Engine Connect*.scwdp.zip",
    # Path to the Sitecore Commerce Experience Accelerator SCWDP file.
    [string]$SXACommerceWdpFullPath = "$XCInstallRoot\Sitecore Commerce Experience Accelerator*.scwdp.zip",
    # Path to the Sitecore Commerce Experience Accelerator Habitat Catalog SCWDP file.
    [string]$SXAStorefrontCatalogWdpFullPath = "$XCInstallRoot\Sitecore Commerce Experience Accelerator Habitat*.scwdp.zip",
    # Path to the Sitecore Commerce Experience Accelerator Storefront SCWDP file.
    [string]$SXAStorefrontWdpFullPath = "$XCInstallRoot\Sitecore Commerce Experience Accelerator Storefront*.scwdp.zip",
    # Path to the Sitecore Commerce Experience Accelerator Storefront Themes SCWDP file.
    [string]$SXAStorefrontThemeWdpFullPath = "$XCInstallRoot\Sitecore Commerce Experience Accelerator Storefront Themes*.scwdp.zip",
    # Path to the Sitecore Commerce Experience Analytics Core SCWDP file.
    [string]$CommercexAnalyticsWdpFullPath = "$XCInstallRoot\Sitecore Commerce ExperienceAnalytics Core*.scwdp.zip",
    # Path to the Sitecore Commerce Experience Profile Core SCWDP file.
    [string]$CommercexProfilesWdpFullPath = "$XCInstallRoot\Sitecore Commerce ExperienceProfile Core*.scwdp.zip",
    # Path to the Sitecore Commerce Marketing Automation Core SCWDP file.
    [string]$CommerceMAWdpFullPath = "$XCInstallRoot\Sitecore Commerce Marketing Automation Core*.scwdp.zip",
    # Path to the Sitecore Commerce Marketing Automation for AutomationEngine zip file.
    [string]$CommerceMAForAutomationEngineZIPFullPath = "$XCInstallRoot\Sitecore Commerce Marketing Automation for AutomationEngine*.zip",
    # Path to the Sitecore BizFx Server SCWDP file.
    [string]$BizFxPackage = "$XCInstallRoot\Sitecore.BizFx.OnPrem*scwdp.zip",
    # Path to the Commerce Engine Service SCWDP file.
    [string]$CommerceEngineWdpFullPath = "$XCInstallRoot\Sitecore.Commerce.Engine.OnPrem.Solr.*scwdp.zip",
    # Path to the Sitecore.Commerce.Habitat.Images.OnPrem SCWDP file.
    [string]$HabitatImagesWdpFullPath = "$XCInstallRoot\Sitecore.Commerce.Habitat.Images.OnPrem.scwdp.zip",

    # The prefix that will be used on SOLR, Website and Database instances. The default value matches the Sitecore XP default.
    [string]$SiteNamePrefix = "storefront1020",
    # The prefix to match Marketing Automation engine service name. Used in form "<MAEnginePrefix>_xconnect-MarketingAutomationService".
    [string]$MAEnginePrefix = "$SiteNamePrefix.local",
    # The name of the Sitecore site instance.
    [string]$SiteName = "$SiteNamePrefix.local",
    # Identity Server site name.
    [string]$IdentityServerSiteName = "$SiteNamePrefix.IdentityServer.local",
    # The url of the Sitecore Identity server.
    [string]$SitecoreIdentityServerUrl = "https://$IdentityServerSiteName",
    # The Commerce Engine Connect Client Id for the Sitecore Identity Server
    [string]$CommerceEngineConnectClientId = "CommerceEngineConnect",
    # The Commerce Engine Connect Client Secret for the Sitecore Identity Server
    [string]$CommerceEngineConnectClientSecret = "hIvJL3QwpGnVT4O83REjChdLkusQJ2cfHrgcb7aSwBo=",
    # The host header name for the Sitecore storefront site.
    [string]$SiteHostHeaderName = "$SiteNamePrefix.local",

    # The path of the Sitecore XP site.
    [string]$InstallDir = "$ScInstallDirectory\$SiteName",
    # The path of the Sitecore XConnect site.
    [string]$XConnectInstallDir = "$ScInstallDirectory\$SiteNamePrefix.xconnect.local",
    # The path to the inetpub folder where Commerce is installed.
    [string]$CommerceInstallRoot = "$ScInstallDirectory",

    # The prefix for Sitecore core and master databases.
    [string]$SqlDbPrefix = $SiteNamePrefix,
    # The location of the database server where Sitecore XP databases are hosted. In case of named SQL instance, use "SQLServerName\\SQLInstanceName"
    [string]$SitecoreDbServer = "localhost",
    # The name of the Sitecore core database.
    [string]$SitecoreCoreDbName = "$($SqlDbPrefix)_Core",
    # A SQL user with sysadmin privileges.
    [string]$SqlUser = "sa",
    # The password for $SQLAdminUser.
    [string]$SqlPass = "Abcd1234",

    # The name of the Sitecore domain.
    [string]$SitecoreDomain = "sitecore",
    # The name of the Sitecore user account.
    [string]$SitecoreUsername = "admin",
    # The password for the $SitecoreUsername.
    [string]$SitecoreUserPassword = "b",

    # The prefix for the Search index. Using the SiteNamePrefix value for the prefix is recommended.
    [string]$SearchIndexPrefix = $SiteNamePrefix,
    # The URL of the Solr Server.
    [string]$SolrUrl = "https://localhost:8988/solr",
    # The folder that Solr has been installed to.
    [string]$SolrRoot = "D:\Solr\Solr-8.8.2",
    # The name of the Solr Service.
    [string]$SolrService = "solr-8.8.2",
    # The prefix for the Storefront index. The default value is the SiteNamePrefix.
    [string]$StorefrontIndexPrefix = $SiteNamePrefix,

    # The host name where Redis is hosted.
    [string]$RedisHost = "localhost",
    # The port number on which Redis is running.
    [string]$RedisPort = "6379",
    # The name of the Redis instance.
    [string]$RedisInstanceName = "Redis",
    # The path to the redis-cli executable.
    [string]$RedisCliPath = "$($Env:SYSTEMDRIVE)\Program Files\Redis\redis-cli.exe",

    # The location of the database server where Commerce databases should be deployed. In case of named SQL instance, use "SQLServerName\\SQLInstanceName"
    [string]$CommerceServicesDbServer = "localhost",
    # The name of the shared database for the Commerce Services.
    [string]$CommerceServicesDbName = "SitecoreCommerce1020_SharedEnvironments",
    # The name of the global database for the Commerce Services.
    [string]$CommerceServicesGlobalDbName = "SitecoreCommerce1020_Global",
    # The name of the archive database for the Commerce Services.
    [string]$CommerceServicesArchiveDbName = "SitecoreCommerce1020_ArchiveSharedEnvironments",    
    # The port for the Commerce Shops Service
    [string]$CommerceShopsServicesPort = "5005",
    # The port for the Commerce Authoring Service.
    [string]$CommerceAuthoringServicesPort = "5000",
    # The port for the Commerce Minions Service.
    [string]$CommerceMinionsServicesPort = "5010",
    # The postfix appended to Commerce services folders names and sitenames.
    # The postfix allows you to host more than one Commerce installment on one server.
    [string]$CommerceServicesPostfix = "xc1020",
    # The postfix used as the root domain name (two-levels) to append as the hostname for Commerce services.
    # By default, all Commerce services are configured as sub-domains of the domain identified by the postfix.
    # Postfix validation enforces the following rules:
    # 1. The first level (TopDomainName) must be 2-7 characters in length and can contain alphabetical characters (a-z, A-Z) only. Numeric and special characters are not valid.
    # 2. The second level (DomainName) can contain alpha-numeric characters (a-z, A-Z,and 0-9) and can include one hyphen (-) character.
    # Special characters (wildcard (*)), for example, are not valid.
    [string]$CommerceServicesHostPostfix = "xc1020.com",

    # The name of the Sitecore XC Business Tools server.
    [string]$BizFxSiteName = "SitecoreBizFx1020",
    # The port of the Sitecore XC Business Tools server.
    [string]$BizFxPort = "4200",

    # The prefix used in the EnvironmentName setting in the config.json file for each Commerce Engine role.
    [string]$EnvironmentsPrefix = "Habitat",
    # The list of Commerce environment names. By default, the script deploys the AdventureWorks and the Habitat environments.
    [array]$Environments = @("AdventureWorksAuthoring", "HabitatAuthoring"),
    # Commerce environments GUIDs used to clean existing Redis cache during deployment. Default parameter values correspond to the default Commerce environment GUIDS.
    [array]$EnvironmentsGuids = @("78a1ea611f3742a7ac899a3f46d60ca5", "40e77b7b4be94186b53b5bfd89a6a83b"),
    # The environments running the minions service. (This is required, for example, for running indexing minions).
    [array]$MinionEnvironments = @("AdventureWorksMinions", "HabitatMinions"),
    # whether to deploy sample data for each environment.
    [bool]$DeploySampleData = $true,

    # The domain of the local account used for the various application pools created as part of the deployment.
    [string]$UserDomain = $Env:COMPUTERNAME,
    # The user name for a local account to be set up for the various application pools that are created as part of the deployment.
    [string]$UserName = "WindowsUserName1",
    # The password for the $UserName.
    [string]$UserPassword = "WindowsUserPassword!",

    # The Braintree Merchant Id.
    [string]$BraintreeMerchantId = "",
    # The Braintree Public Key.
    [string]$BraintreePublicKey = "",
    # The Braintree Private Key.
    [string]$BraintreePrivateKey = "",
    # The Braintree Environment.
    [string]$BraintreeEnvironment = "",

    # List of comma-separated task names to skip during Sitecore XC deployment.
    [string]$TasksToSkip = ""
)

Function Resolve-ItemPath {
    param (
        [Parameter(Mandatory = $true)]
        [ValidateNotNullorEmpty()]
        [string] $Path
    )
    process {
        if ([string]::IsNullOrWhiteSpace($Path)) {
            throw "Parameter could not be validated because it contains only whitespace. Please check script parameters."
        }
        $itemPath = Resolve-Path -Path $Path -ErrorAction SilentlyContinue | Select-Object -First 1
        if ([string]::IsNullOrEmpty($itemPath) -or (-not (Test-Path $itemPath))) {
            throw "Path [$Path] could not be resolved. Please check script parameters."
        }

        Write-Host "Found [$itemPath]."
        return $itemPath
    }
}

if (($SkipDeployStorefrontPackages -eq $true) -and ($SkipInstallDefaultStorefront -eq $false)) {
    throw "You cannot install the SXA Storefront without deploying necessary packages. If you want to install the SXA Storefront, set [SkipDeployStorefrontPackages] parameter to [false]."
}

if (($DeploySampleData -eq $false) -and ($SkipInstallDefaultStorefront -eq $false)) {
    throw "You cannot install the SXA Storefront without deploying sample data. If you want to install the SXA Storefront, set [DeploySampleData] parameter to [true]."
}

[string[]] $Skip = @()
if (-not ([string]::IsNullOrWhiteSpace($TasksToSkip))) {
    $TasksToSkip.Split(',') | ForEach-Object { $Skip += $_.Trim() }
}

Push-Location $PSScriptRoot

$modulesPath = ( Join-Path -Path $PWD -ChildPath "Modules" )
if ($env:PSModulePath -notlike "*$modulesPath*") {
    [Environment]::SetEnvironmentVariable("PSModulePath", "$env:PSModulePath;$modulesPath")
}

$deployCommerceParams = @{
    Path                                     = Resolve-ItemPath -Path $Path
    SolrSchemas                              = Resolve-ItemPath -Path $SolrSchemas
    SiteUtilitiesSrc                         = Resolve-ItemPath -Path $SiteUtilitiesSrc
    MergeToolFullPath                        = Resolve-ItemPath -Path $MergeToolFullPath
    AdventureWorksImagesWdpFullPath          = Resolve-ItemPath -Path $AdventureWorksImagesWdpFullPath
    CommerceConnectWdpFullPath               = Resolve-ItemPath -Path $CommerceConnectWdpFullPath
    CEConnectWdpFullPath                     = Resolve-ItemPath -Path $CEConnectWdpFullPath
    SXACommerceWdpFullPath                   = Resolve-ItemPath -Path $SXACommerceWdpFullPath
    SXAStorefrontCatalogWdpFullPath          = Resolve-ItemPath -Path $SXAStorefrontCatalogWdpFullPath
    SXAStorefrontWdpFullPath                 = Resolve-ItemPath -Path $SXAStorefrontWdpFullPath
    SXAStorefrontThemeWdpFullPath            = Resolve-ItemPath -Path $SXAStorefrontThemeWdpFullPath
    CommercexAnalyticsWdpFullPath            = Resolve-ItemPath -Path $CommercexAnalyticsWdpFullPath
    CommercexProfilesWdpFullPath             = Resolve-ItemPath -Path $CommercexProfilesWdpFullPath
    CommerceMAWdpFullPath                    = Resolve-ItemPath -Path $CommerceMAWdpFullPath
    CommerceMAForAutomationEngineZIPFullPath = Resolve-ItemPath -Path $CommerceMAForAutomationEngineZIPFullPath
    BizFxPackage                             = Resolve-ItemPath -Path $BizFxPackage
    CommerceEngineWdpFullPath                = Resolve-ItemPath -Path $CommerceEngineWdpFullPath
    HabitatImagesWdpFullPath                 = Resolve-ItemPath -Path $HabitatImagesWdpFullPath
    SiteName                                 = $SiteName
    MAEnginePrefix                           = $MAEnginePrefix
    SiteHostHeaderName                       = $SiteHostHeaderName
    InstallDir                               = Resolve-ItemPath -Path $InstallDir
    XConnectInstallDir                       = Resolve-ItemPath -Path $XConnectInstallDir
    CommerceInstallRoot                      = Resolve-ItemPath -Path $CommerceInstallRoot
    CommerceServicesDbServer                 = $CommerceServicesDbServer
    CommerceServicesDbName                   = $CommerceServicesDbName
    CommerceServicesGlobalDbName             = $CommerceServicesGlobalDbName
    CommerceServicesArchiveDbName            = $CommerceServicesArchiveDbName
    SitecoreDbServer                         = $SitecoreDbServer
    SitecoreCoreDbName                       = $SitecoreCoreDbName
    SqlDbPrefix                              = $SqlDbPrefix
    SqlAdminUser                             = $SqlUser
    SqlAdminPassword                         = $SqlPass
    SolrUrl                                  = $SolrUrl
    SolrRoot                                 = Resolve-ItemPath -Path $SolrRoot
    SolrService                              = $SolrService
    SearchIndexPrefix                        = $SearchIndexPrefix
    StorefrontIndexPrefix                    = $StorefrontIndexPrefix
    CommerceServicesPostfix                  = $CommerceServicesPostfix
    CommerceServicesHostPostfix              = $CommerceServicesHostPostfix
    EnvironmentsPrefix                       = $EnvironmentsPrefix
    Environments                             = $Environments
    EnvironmentsGuids                        = $EnvironmentsGuids
    MinionEnvironments                       = $MinionEnvironments    
    CommerceShopsServicesPort                = $CommerceShopsServicesPort
    CommerceAuthoringServicesPort            = $CommerceAuthoringServicesPort
    CommerceMinionsServicesPort              = $CommerceMinionsServicesPort
    RedisInstanceName                        = $RedisInstanceName
    RedisCliPath                             = $RedisCliPath
    RedisHost                                = $RedisHost
    RedisPort                                = $RedisPort
    UserDomain                               = $UserDomain
    UserName                                 = $UserName
    UserPassword                             = $UserPassword
    BraintreeMerchantId                      = $BraintreeMerchantId
    BraintreePublicKey                       = $BraintreePublicKey
    BraintreePrivateKey                      = $BraintreePrivateKey
    BraintreeEnvironment                     = $BraintreeEnvironment
    SitecoreDomain                           = $SitecoreDomain
    SitecoreUsername                         = $SitecoreUsername
    SitecoreUserPassword                     = $SitecoreUserPassword
    BizFxSiteName                            = $BizFxSiteName
    BizFxPort                                = $BizFxPort
    SitecoreIdentityServerApplicationName    = $IdentityServerSiteName
    SitecoreIdentityServerUrl                = $SitecoreIdentityServerUrl
    SkipInstallDefaultStorefront             = $SkipInstallDefaultStorefront
    SkipDeployStorefrontPackages             = $SkipDeployStorefrontPackages
    CommerceEngineConnectClientId            = $CommerceEngineConnectClientId
    CommerceEngineConnectClientSecret        = $CommerceEngineConnectClientSecret
    DeploySampleData                         = $DeploySampleData
}

if ($Skip.Count -eq 0) {
    #Install-SitecoreConfiguration @deployCommerceParams -Verbose *>&1 | Tee-Object "$XCSIFInstallRoot\XC-Install.log"
	
	# Uncomment the below line and comment out the above if you want to remove the Sitecore XC
	Uninstall-SitecoreConfiguration @deployCommerceParams -Verbose *>&1 | Tee-Object "$XCSIFInstallRoot\XC-Install.log"
}
else {
    Install-SitecoreConfiguration @deployCommerceParams -Skip $Skip -Verbose *>&1 | Tee-Object "$XCSIFInstallRoot\XC-Install.log"
}

# SIG # Begin signature block
# MIIl4gYJKoZIhvcNAQcCoIIl0zCCJc8CAQExDzANBglghkgBZQMEAgEFADB5Bgor
# BgEEAYI3AgEEoGswaTA0BgorBgEEAYI3AgEeMCYCAwEAAAQQH8w7YFlLCE63JNLG
# KX7zUQIBAAIBAAIBAAIBAAIBADAxMA0GCWCGSAFlAwQCAQUABCDtOgODZfm6YUCL
# /rabUlkXeQ8urRrArFH4MOzThGbgQqCCE8kwggWQMIIDeKADAgECAhAFmxtXno4h
# MuI5B72nd3VcMA0GCSqGSIb3DQEBDAUAMGIxCzAJBgNVBAYTAlVTMRUwEwYDVQQK
# EwxEaWdpQ2VydCBJbmMxGTAXBgNVBAsTEHd3dy5kaWdpY2VydC5jb20xITAfBgNV
# BAMTGERpZ2lDZXJ0IFRydXN0ZWQgUm9vdCBHNDAeFw0xMzA4MDExMjAwMDBaFw0z
# ODAxMTUxMjAwMDBaMGIxCzAJBgNVBAYTAlVTMRUwEwYDVQQKEwxEaWdpQ2VydCBJ
# bmMxGTAXBgNVBAsTEHd3dy5kaWdpY2VydC5jb20xITAfBgNVBAMTGERpZ2lDZXJ0
# IFRydXN0ZWQgUm9vdCBHNDCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIB
# AL/mkHNo3rvkXUo8MCIwaTPswqclLskhPfKK2FnC4SmnPVirdprNrnsbhA3EMB/z
# G6Q4FutWxpdtHauyefLKEdLkX9YFPFIPUh/GnhWlfr6fqVcWWVVyr2iTcMKyunWZ
# anMylNEQRBAu34LzB4TmdDttceItDBvuINXJIB1jKS3O7F5OyJP4IWGbNOsFxl7s
# Wxq868nPzaw0QF+xembud8hIqGZXV59UWI4MK7dPpzDZVu7Ke13jrclPXuU15zHL
# 2pNe3I6PgNq2kZhAkHnDeMe2scS1ahg4AxCN2NQ3pC4FfYj1gj4QkXCrVYJBMtfb
# BHMqbpEBfCFM1LyuGwN1XXhm2ToxRJozQL8I11pJpMLmqaBn3aQnvKFPObURWBf3
# JFxGj2T3wWmIdph2PVldQnaHiZdpekjw4KISG2aadMreSx7nDmOu5tTvkpI6nj3c
# AORFJYm2mkQZK37AlLTSYW3rM9nF30sEAMx9HJXDj/chsrIRt7t/8tWMcCxBYKqx
# YxhElRp2Yn72gLD76GSmM9GJB+G9t+ZDpBi4pncB4Q+UDCEdslQpJYls5Q5SUUd0
# viastkF13nqsX40/ybzTQRESW+UQUOsxxcpyFiIJ33xMdT9j7CFfxCBRa2+xq4aL
# T8LWRV+dIPyhHsXAj6KxfgommfXkaS+YHS312amyHeUbAgMBAAGjQjBAMA8GA1Ud
# EwEB/wQFMAMBAf8wDgYDVR0PAQH/BAQDAgGGMB0GA1UdDgQWBBTs1+OC0nFdZEzf
# Lmc/57qYrhwPTzANBgkqhkiG9w0BAQwFAAOCAgEAu2HZfalsvhfEkRvDoaIAjeNk
# aA9Wz3eucPn9mkqZucl4XAwMX+TmFClWCzZJXURj4K2clhhmGyMNPXnpbWvWVPjS
# PMFDQK4dUPVS/JA7u5iZaWvHwaeoaKQn3J35J64whbn2Z006Po9ZOSJTROvIXQPK
# 7VB6fWIhCoDIc2bRoAVgX+iltKevqPdtNZx8WorWojiZ83iL9E3SIAveBO6Mm0eB
# cg3AFDLvMFkuruBx8lbkapdvklBtlo1oepqyNhR6BvIkuQkRUNcIsbiJeoQjYUIp
# 5aPNoiBB19GcZNnqJqGLFNdMGbJQQXE9P01wI4YMStyB0swylIQNCAmXHE/A7msg
# dDDS4Dk0EIUhFQEI6FUy3nFJ2SgXUE3mvk3RdazQyvtBuEOlqtPDBURPLDab4vri
# RbgjU2wGb2dVf0a1TD9uKFp5JtKkqGKX0h7i7UqLvBv9R0oN32dmfrJbQdA75PQ7
# 9ARj6e/CVABRoIoqyc54zNXqhwQYs86vSYiv85KZtrPmYQ/ShQDnUBrkG5WdGaG5
# nLGbsQAe79APT0JsyQq87kP6OnGlyE0mpTX9iV28hWIdMtKgK1TtmlfB2/oQzxm3
# i0objwG2J5VT6LaJbVu8aNQj6ItRolb58KaAoNYes7wPD1N1KarqE3fk3oyBIa0H
# EEcRrYc9B9F1vM/zZn4wggawMIIEmKADAgECAhAIrUCyYNKcTJ9ezam9k67ZMA0G
# CSqGSIb3DQEBDAUAMGIxCzAJBgNVBAYTAlVTMRUwEwYDVQQKEwxEaWdpQ2VydCBJ
# bmMxGTAXBgNVBAsTEHd3dy5kaWdpY2VydC5jb20xITAfBgNVBAMTGERpZ2lDZXJ0
# IFRydXN0ZWQgUm9vdCBHNDAeFw0yMTA0MjkwMDAwMDBaFw0zNjA0MjgyMzU5NTla
# MGkxCzAJBgNVBAYTAlVTMRcwFQYDVQQKEw5EaWdpQ2VydCwgSW5jLjFBMD8GA1UE
# AxM4RGlnaUNlcnQgVHJ1c3RlZCBHNCBDb2RlIFNpZ25pbmcgUlNBNDA5NiBTSEEz
# ODQgMjAyMSBDQTEwggIiMA0GCSqGSIb3DQEBAQUAA4ICDwAwggIKAoICAQDVtC9C
# 0CiteLdd1TlZG7GIQvUzjOs9gZdwxbvEhSYwn6SOaNhc9es0JAfhS0/TeEP0F9ce
# 2vnS1WcaUk8OoVf8iJnBkcyBAz5NcCRks43iCH00fUyAVxJrQ5qZ8sU7H/Lvy0da
# E6ZMswEgJfMQ04uy+wjwiuCdCcBlp/qYgEk1hz1RGeiQIXhFLqGfLOEYwhrMxe6T
# SXBCMo/7xuoc82VokaJNTIIRSFJo3hC9FFdd6BgTZcV/sk+FLEikVoQ11vkunKoA
# FdE3/hoGlMJ8yOobMubKwvSnowMOdKWvObarYBLj6Na59zHh3K3kGKDYwSNHR7Oh
# D26jq22YBoMbt2pnLdK9RBqSEIGPsDsJ18ebMlrC/2pgVItJwZPt4bRc4G/rJvmM
# 1bL5OBDm6s6R9b7T+2+TYTRcvJNFKIM2KmYoX7BzzosmJQayg9Rc9hUZTO1i4F4z
# 8ujo7AqnsAMrkbI2eb73rQgedaZlzLvjSFDzd5Ea/ttQokbIYViY9XwCFjyDKK05
# huzUtw1T0PhH5nUwjewwk3YUpltLXXRhTT8SkXbev1jLchApQfDVxW0mdmgRQRNY
# mtwmKwH0iU1Z23jPgUo+QEdfyYFQc4UQIyFZYIpkVMHMIRroOBl8ZhzNeDhFMJlP
# /2NPTLuqDQhTQXxYPUez+rbsjDIJAsxsPAxWEQIDAQABo4IBWTCCAVUwEgYDVR0T
# AQH/BAgwBgEB/wIBADAdBgNVHQ4EFgQUaDfg67Y7+F8Rhvv+YXsIiGX0TkIwHwYD
# VR0jBBgwFoAU7NfjgtJxXWRM3y5nP+e6mK4cD08wDgYDVR0PAQH/BAQDAgGGMBMG
# A1UdJQQMMAoGCCsGAQUFBwMDMHcGCCsGAQUFBwEBBGswaTAkBggrBgEFBQcwAYYY
# aHR0cDovL29jc3AuZGlnaWNlcnQuY29tMEEGCCsGAQUFBzAChjVodHRwOi8vY2Fj
# ZXJ0cy5kaWdpY2VydC5jb20vRGlnaUNlcnRUcnVzdGVkUm9vdEc0LmNydDBDBgNV
# HR8EPDA6MDigNqA0hjJodHRwOi8vY3JsMy5kaWdpY2VydC5jb20vRGlnaUNlcnRU
# cnVzdGVkUm9vdEc0LmNybDAcBgNVHSAEFTATMAcGBWeBDAEDMAgGBmeBDAEEATAN
# BgkqhkiG9w0BAQwFAAOCAgEAOiNEPY0Idu6PvDqZ01bgAhql+Eg08yy25nRm95Ry
# sQDKr2wwJxMSnpBEn0v9nqN8JtU3vDpdSG2V1T9J9Ce7FoFFUP2cvbaF4HZ+N3HL
# IvdaqpDP9ZNq4+sg0dVQeYiaiorBtr2hSBh+3NiAGhEZGM1hmYFW9snjdufE5Btf
# Q/g+lP92OT2e1JnPSt0o618moZVYSNUa/tcnP/2Q0XaG3RywYFzzDaju4ImhvTnh
# OE7abrs2nfvlIVNaw8rpavGiPttDuDPITzgUkpn13c5UbdldAhQfQDN8A+KVssIh
# dXNSy0bYxDQcoqVLjc1vdjcshT8azibpGL6QB7BDf5WIIIJw8MzK7/0pNVwfiThV
# 9zeKiwmhywvpMRr/LhlcOXHhvpynCgbWJme3kuZOX956rEnPLqR0kq3bPKSchh/j
# wVYbKyP/j7XqiHtwa+aguv06P0WmxOgWkVKLQcBIhEuWTatEQOON8BUozu3xGFYH
# Ki8QxAwIZDwzj64ojDzLj4gLDb879M4ee47vtevLt/B3E+bnKD+sEq6lLyJsQfmC
# XBVmzGwOysWGw/YmMwwHS6DTBwJqakAwSEs0qFEgu60bhQjiWQ1tygVQK+pKHJ6l
# /aCnHwZ05/LWUpD9r4VIIflXO7ScA+2GRfS0YW6/aOImYIbqyK+p/pQd52MbOoZW
# eE4wggd9MIIFZaADAgECAhAEe51QLiqN2yKyU0tANZXhMA0GCSqGSIb3DQEBCwUA
# MGkxCzAJBgNVBAYTAlVTMRcwFQYDVQQKEw5EaWdpQ2VydCwgSW5jLjFBMD8GA1UE
# AxM4RGlnaUNlcnQgVHJ1c3RlZCBHNCBDb2RlIFNpZ25pbmcgUlNBNDA5NiBTSEEz
# ODQgMjAyMSBDQTEwHhcNMjExMDI2MDAwMDAwWhcNMjIxMTAyMjM1OTU5WjCBgTEL
# MAkGA1UEBhMCVVMxEzARBgNVBAgTCkNhbGlmb3JuaWExFjAUBgNVBAcTDVNhbiBG
# cmFuY2lzY28xGzAZBgNVBAoTElNpdGVjb3JlIFVTQSwgSW5jLjELMAkGA1UECxMC
# SVQxGzAZBgNVBAMTElNpdGVjb3JlIFVTQSwgSW5jLjCCAiIwDQYJKoZIhvcNAQEB
# BQADggIPADCCAgoCggIBALqIAka1Ql+aPWB4e8acNTR7oGt0JAAjkKxu9dhQnuh+
# DaQePslSzOXcgPgb64FJYjpHdi4s1Kt1jTgN7FFjiT3YQqDYCxv26s0EbZKSNwmN
# KlSIQDujSriirCoA6oLd8uI6dXblbPmd5xw3frkWgTc2ILqAIuo3bgHQ80AvF3pS
# 8OtXmxPNwnJ9N+PuhjG2xCDUGorEhgAtfqxb9C/6k6hZAFoufxV2ctSKWDWLAt6x
# J4eYqnr0Vn+jmfj5xttozxEdn/J3E7Qz+Sewz/i8LqE9fa6xKwNczBlZA9UKdDj4
# cGJJs/nujfq6TUTyqH8eLzi2KG4DynpQ3yWyw0e2qqI11fbaplSpj7ZnapgOiyTb
# l4Vgayibdad5TpOxpHZvmw5OcYauNUK1gOGBh1VmG0M83Nl4Z5RflTWOjaW/j0FT
# nzCjjfX16xTelkDIXMo3oxGrj/qnFkWYucUNlhXz23HNyGj0vR/FD2kQXBU3gLxm
# JkaJ9H4vwVVWPSc7g8B4NQ8GGrGny5GGbcmL5eODGYS4+S6Zi80ecUQXczXIOo4s
# Osu2JqQRPST671Gigh7Aau7KLquW9/QwlMokNCRMKYSNQd5+JP73yZN2eRkDWrcg
# DCSdFfesBvacb3Fa4YhhnM3kcmWt8Z+e0w8eHYft2Jz7OpqUxMS/fHc4AmcMagpp
# AgMBAAGjggIGMIICAjAfBgNVHSMEGDAWgBRoN+Drtjv4XxGG+/5hewiIZfROQjAd
# BgNVHQ4EFgQUZqXdPbubXiqtLH7KbaP0G/WlYHgwDgYDVR0PAQH/BAQDAgeAMBMG
# A1UdJQQMMAoGCCsGAQUFBwMDMIG1BgNVHR8Ega0wgaowU6BRoE+GTWh0dHA6Ly9j
# cmwzLmRpZ2ljZXJ0LmNvbS9EaWdpQ2VydFRydXN0ZWRHNENvZGVTaWduaW5nUlNB
# NDA5NlNIQTM4NDIwMjFDQTEuY3JsMFOgUaBPhk1odHRwOi8vY3JsNC5kaWdpY2Vy
# dC5jb20vRGlnaUNlcnRUcnVzdGVkRzRDb2RlU2lnbmluZ1JTQTQwOTZTSEEzODQy
# MDIxQ0ExLmNybDA+BgNVHSAENzA1MDMGBmeBDAEEATApMCcGCCsGAQUFBwIBFhto
# dHRwOi8vd3d3LmRpZ2ljZXJ0LmNvbS9DUFMwgZQGCCsGAQUFBwEBBIGHMIGEMCQG
# CCsGAQUFBzABhhhodHRwOi8vb2NzcC5kaWdpY2VydC5jb20wXAYIKwYBBQUHMAKG
# UGh0dHA6Ly9jYWNlcnRzLmRpZ2ljZXJ0LmNvbS9EaWdpQ2VydFRydXN0ZWRHNENv
# ZGVTaWduaW5nUlNBNDA5NlNIQTM4NDIwMjFDQTEuY3J0MAwGA1UdEwEB/wQCMAAw
# DQYJKoZIhvcNAQELBQADggIBAGlm35F2ALzbmAAPmi+UUoGnLCpJX0TE3PHXMcR5
# 9mGZHxGfrDkHcWzDK9zGfqTujANHvX8CDRacdytsj87SzAHPP6kC53XNHkh5OPE6
# 1Wb58sqU1rqJVopoWX0NH7BSwk5TDFp5uM44olz+Ra0/h4uo2CD9fBYPgToA8Wkx
# lhSg7/it2osE4MzAJRUVMPwO8AmmJjtKND9FMWG3ceKTwWltDWhsYVbO/uzp7nTq
# FziS2fk0jMKP8ZhWihPotySYU7rvq9hr9qsYzhqJmDIqZrz5VX7WIckTJzA0tOyZ
# udkIEh3y/Nlh6spVVVGRy4sPKGc3v4LchuWPVQ1UGWoZd+uOubgl9diBezCKnDGd
# KvwCaWVYI/5bAzTWNkJURDO2FrpPx7iQOu/Xzds/Y0iLYehuyNAec6EOJNQyd29Q
# gsPFutersW3LwbDVtdqCjf+nk/0pnP2fZd4e7nOg63DS/WQKMFzEAqlnDWA7alAl
# BjyWjhyoiywq0sp0dNUUW6QbO56JnmVKnpA4iE095F5pIDOJkaS1pvcYYQ+RlrsN
# 99tVRjqbiIiBrB1Q0+FER6RyTDa368G19Pdg7jbms1OtZnPzXRlZ4+rNRK7nE22Q
# 5dyxtzlYy/Sv377tK05LxVKlzS3AXTos6Jm66eiiWvZN+wrsEIc7ChLiw+lwVhy0
# ExqYMYIRbzCCEWsCAQEwfTBpMQswCQYDVQQGEwJVUzEXMBUGA1UEChMORGlnaUNl
# cnQsIEluYy4xQTA/BgNVBAMTOERpZ2lDZXJ0IFRydXN0ZWQgRzQgQ29kZSBTaWdu
# aW5nIFJTQTQwOTYgU0hBMzg0IDIwMjEgQ0ExAhAEe51QLiqN2yKyU0tANZXhMA0G
# CWCGSAFlAwQCAQUAoIGEMBgGCisGAQQBgjcCAQwxCjAIoAKAAKECgAAwGQYJKoZI
# hvcNAQkDMQwGCisGAQQBgjcCAQQwHAYKKwYBBAGCNwIBCzEOMAwGCisGAQQBgjcC
# ARUwLwYJKoZIhvcNAQkEMSIEIDDsGL9j9KNQ12zXsiDPxpIQ69/nrmVu7Luh9o8B
# LRhnMA0GCSqGSIb3DQEBAQUABIICAGDkJ7G+fnR3U0lvU8v2r/hd1l53j42jmas9
# vM5EKZY8ZmgL3okINUHGVmOi7MBa/deXuBYYEV+5AFTIgNnsk4ktYJhEfu7zQK7U
# FDX3PytVo9MU/kuNPNFt2O0v11p0YbkOZX3QWSvvfrbi0yMdw3dR6AeymS2xz3LU
# q5KWfnDHsPRMr6yLRvYP5BUUfGeWDysWDAVL2+Tx14G2KAGKD3898jN7C5PDIaCx
# S5rFffT+dlhV/PbqacC38deSLqPgewWjxF1xIrqVIEYkti8OJtOSiVSgY+3Sr3ri
# Rzk7mTsgrd42NfoKAM/MUplnvqD/9884lHPBGPZyZs0b4IqvyxAnWvs8qOcaMpy2
# o4h+TpphVn9FhtaKnJ4eqUJ4e2WpZOk54zGM4r16UHu4F3GSaBBbb3JCIO7uSygy
# E8j0z95U7EEVkw6i9hN2xfISvU3nq8lgj7xMAENYFnUcOOOz7La/mSG0IA+qlE/6
# Cj6ijfQA0dj/LYApbeQStw2ornin4BXWkaJ6LuMF/c2iFVqvTBIJseVfWsX3QuoA
# CSYII4mvcEfcfkjqF7+qYDusBJSpuqlowHb8N5MHdQ48sfgFd4rlB8uwwQ58J4QJ
# 1tnekx4MgJZOE7ATv3n5suXCE7alg6SQ3DUz8txHYMQUKyhXZKYImx6yuhbj7oyg
# cDtFZ2+CoYIOPDCCDjgGCisGAQQBgjcDAwExgg4oMIIOJAYJKoZIhvcNAQcCoIIO
# FTCCDhECAQMxDTALBglghkgBZQMEAgEwggEOBgsqhkiG9w0BCRABBKCB/gSB+zCB
# +AIBAQYLYIZIAYb4RQEHFwMwMTANBglghkgBZQMEAgEFAAQgMuNasXniu3n0hrsL
# zuNUD2UbtMwlIH2KFPIgKbe61SECFEB9K5zSuefSiRDkiOBMuc5R7FejGA8yMDIx
# MTEwMzE3MzAwNlowAwIBHqCBhqSBgzCBgDELMAkGA1UEBhMCVVMxHTAbBgNVBAoT
# FFN5bWFudGVjIENvcnBvcmF0aW9uMR8wHQYDVQQLExZTeW1hbnRlYyBUcnVzdCBO
# ZXR3b3JrMTEwLwYDVQQDEyhTeW1hbnRlYyBTSEEyNTYgVGltZVN0YW1waW5nIFNp
# Z25lciAtIEczoIIKizCCBTgwggQgoAMCAQICEHsFsdRJaFFE98mJ0pwZnRIwDQYJ
# KoZIhvcNAQELBQAwgb0xCzAJBgNVBAYTAlVTMRcwFQYDVQQKEw5WZXJpU2lnbiwg
# SW5jLjEfMB0GA1UECxMWVmVyaVNpZ24gVHJ1c3QgTmV0d29yazE6MDgGA1UECxMx
# KGMpIDIwMDggVmVyaVNpZ24sIEluYy4gLSBGb3IgYXV0aG9yaXplZCB1c2Ugb25s
# eTE4MDYGA1UEAxMvVmVyaVNpZ24gVW5pdmVyc2FsIFJvb3QgQ2VydGlmaWNhdGlv
# biBBdXRob3JpdHkwHhcNMTYwMTEyMDAwMDAwWhcNMzEwMTExMjM1OTU5WjB3MQsw
# CQYDVQQGEwJVUzEdMBsGA1UEChMUU3ltYW50ZWMgQ29ycG9yYXRpb24xHzAdBgNV
# BAsTFlN5bWFudGVjIFRydXN0IE5ldHdvcmsxKDAmBgNVBAMTH1N5bWFudGVjIFNI
# QTI1NiBUaW1lU3RhbXBpbmcgQ0EwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEK
# AoIBAQC7WZ1ZVU+djHJdGoGi61XzsAGtPHGsMo8Fa4aaJwAyl2pNyWQUSym7wtkp
# uS7sY7Phzz8LVpD4Yht+66YH4t5/Xm1AONSRBudBfHkcy8utG7/YlZHz8O5s+K2W
# OS5/wSe4eDnFhKXt7a+Hjs6Nx23q0pi1Oh8eOZ3D9Jqo9IThxNF8ccYGKbQ/5IMN
# JsN7CD5N+Qq3M0n/yjvU9bKbS+GImRr1wOkzFNbfx4Dbke7+vJJXcnf0zajM/gn1
# kze+lYhqxdz0sUvUzugJkV+1hHk1inisGTKPI8EyQRtZDqk+scz51ivvt9jk1R1t
# ETqS9pPJnONI7rtTDtQ2l4Z4xaE3AgMBAAGjggF3MIIBczAOBgNVHQ8BAf8EBAMC
# AQYwEgYDVR0TAQH/BAgwBgEB/wIBADBmBgNVHSAEXzBdMFsGC2CGSAGG+EUBBxcD
# MEwwIwYIKwYBBQUHAgEWF2h0dHBzOi8vZC5zeW1jYi5jb20vY3BzMCUGCCsGAQUF
# BwICMBkaF2h0dHBzOi8vZC5zeW1jYi5jb20vcnBhMC4GCCsGAQUFBwEBBCIwIDAe
# BggrBgEFBQcwAYYSaHR0cDovL3Muc3ltY2QuY29tMDYGA1UdHwQvMC0wK6ApoCeG
# JWh0dHA6Ly9zLnN5bWNiLmNvbS91bml2ZXJzYWwtcm9vdC5jcmwwEwYDVR0lBAww
# CgYIKwYBBQUHAwgwKAYDVR0RBCEwH6QdMBsxGTAXBgNVBAMTEFRpbWVTdGFtcC0y
# MDQ4LTMwHQYDVR0OBBYEFK9j1sqjToVy4Ke8QfMpojh/gHViMB8GA1UdIwQYMBaA
# FLZ3+mlIR59TEtXC6gcydgfRlwcZMA0GCSqGSIb3DQEBCwUAA4IBAQB16rAt1TQZ
# XDJF/g7h1E+meMFv1+rd3E/zociBiPenjxXmQCmt5l30otlWZIRxMCrdHmEXZiBW
# BpgZjV1x8viXvAn9HJFHyeLojQP7zJAv1gpsTjPs1rSTyEyQY0g5QCHE3dZuiZg8
# tZiX6KkGtwnJj1NXQZAv4R5NTtzKEHhsQm7wtsX4YVxS9U72a433Snq+8839A9fZ
# 9gOoD+NT9wp17MZ1LqpmhQSZt/gGV+HGDvbor9rsmxgfqrnjOgC/zoqUywHbnsc4
# uw9Sq9HjlANgCk2g/idtFDL8P5dA4b+ZidvkORS92uTTw+orWrOVWFUEfcea7CMD
# jYUq0v+uqWGBMIIFSzCCBDOgAwIBAgIQe9Tlr7rMBz+hASMEIkFNEjANBgkqhkiG
# 9w0BAQsFADB3MQswCQYDVQQGEwJVUzEdMBsGA1UEChMUU3ltYW50ZWMgQ29ycG9y
# YXRpb24xHzAdBgNVBAsTFlN5bWFudGVjIFRydXN0IE5ldHdvcmsxKDAmBgNVBAMT
# H1N5bWFudGVjIFNIQTI1NiBUaW1lU3RhbXBpbmcgQ0EwHhcNMTcxMjIzMDAwMDAw
# WhcNMjkwMzIyMjM1OTU5WjCBgDELMAkGA1UEBhMCVVMxHTAbBgNVBAoTFFN5bWFu
# dGVjIENvcnBvcmF0aW9uMR8wHQYDVQQLExZTeW1hbnRlYyBUcnVzdCBOZXR3b3Jr
# MTEwLwYDVQQDEyhTeW1hbnRlYyBTSEEyNTYgVGltZVN0YW1waW5nIFNpZ25lciAt
# IEczMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArw6Kqvjcv2l7VBdx
# Rwm9jTyB+HQVd2eQnP3eTgKeS3b25TY+ZdUkIG0w+d0dg+k/J0ozTm0WiuSNQI0i
# qr6nCxvSB7Y8tRokKPgbclE9yAmIJgg6+fpDI3VHcAyzX1uPCB1ySFdlTa8CPED3
# 9N0yOJM/5Sym81kjy4DeE035EMmqChhsVWFX0fECLMS1q/JsI9KfDQ8ZbK2FYmn9
# ToXBilIxq1vYyXRS41dsIr9Vf2/KBqs/SrcidmXs7DbylpWBJiz9u5iqATjTryVA
# mwlT8ClXhVhe6oVIQSGH5d600yaye0BTWHmOUjEGTZQDRcTOPAPstwDyOiLFtG/l
# 77CKmwIDAQABo4IBxzCCAcMwDAYDVR0TAQH/BAIwADBmBgNVHSAEXzBdMFsGC2CG
# SAGG+EUBBxcDMEwwIwYIKwYBBQUHAgEWF2h0dHBzOi8vZC5zeW1jYi5jb20vY3Bz
# MCUGCCsGAQUFBwICMBkaF2h0dHBzOi8vZC5zeW1jYi5jb20vcnBhMEAGA1UdHwQ5
# MDcwNaAzoDGGL2h0dHA6Ly90cy1jcmwud3Muc3ltYW50ZWMuY29tL3NoYTI1Ni10
# c3MtY2EuY3JsMBYGA1UdJQEB/wQMMAoGCCsGAQUFBwMIMA4GA1UdDwEB/wQEAwIH
# gDB3BggrBgEFBQcBAQRrMGkwKgYIKwYBBQUHMAGGHmh0dHA6Ly90cy1vY3NwLndz
# LnN5bWFudGVjLmNvbTA7BggrBgEFBQcwAoYvaHR0cDovL3RzLWFpYS53cy5zeW1h
# bnRlYy5jb20vc2hhMjU2LXRzcy1jYS5jZXIwKAYDVR0RBCEwH6QdMBsxGTAXBgNV
# BAMTEFRpbWVTdGFtcC0yMDQ4LTYwHQYDVR0OBBYEFKUTAamfhcwbbhYeXzsxqnk2
# AHsdMB8GA1UdIwQYMBaAFK9j1sqjToVy4Ke8QfMpojh/gHViMA0GCSqGSIb3DQEB
# CwUAA4IBAQBGnq/wuKJfoplIz6gnSyHNsrmmcnBjL+NVKXs5Rk7nfmUGWIu8V4qS
# DQjYELo2JPoKe/s702K/SpQV5oLbilRt/yj+Z89xP+YzCdmiWRD0Hkr+Zcze1Gvj
# Uil1AEorpczLm+ipTfe0F1mSQcO3P4bm9sB/RDxGXBda46Q71Wkm1SF94YBnfmKs
# t04uFZrlnCOvWxHqcalB+Q15OKmhDc+0sdo+mnrHIsV0zd9HCYbE/JElshuW6YUI
# 6N3qdGBuYKVWeg3IRFjc5vlIFJ7lv94AvXexmBRyFCTfxxEsHwA/w0sUxmcczB4G
# o5BfXFSLPuMzW4IPxbeGAk5xn+lmRT92MYICWjCCAlYCAQEwgYswdzELMAkGA1UE
# BhMCVVMxHTAbBgNVBAoTFFN5bWFudGVjIENvcnBvcmF0aW9uMR8wHQYDVQQLExZT
# eW1hbnRlYyBUcnVzdCBOZXR3b3JrMSgwJgYDVQQDEx9TeW1hbnRlYyBTSEEyNTYg
# VGltZVN0YW1waW5nIENBAhB71OWvuswHP6EBIwQiQU0SMAsGCWCGSAFlAwQCAaCB
# pDAaBgkqhkiG9w0BCQMxDQYLKoZIhvcNAQkQAQQwHAYJKoZIhvcNAQkFMQ8XDTIx
# MTEwMzE3MzAwNlowLwYJKoZIhvcNAQkEMSIEIBQFj4YPBgMjZOs1AwUm2IQGpfZe
# CfpHkKxWCPb+w9V2MDcGCyqGSIb3DQEJEAIvMSgwJjAkMCIEIMR0znYAfQI5Tg2l
# 5N58FMaA+eKCATz+9lPvXbcf32H4MAsGCSqGSIb3DQEBAQSCAQA8a7TuoRCH3q1A
# t++lUzCLe8En0fsppqQEZ7dEvcqA9KzbufbS/q4bRSyepJkrb7Zom4eAwEFiXGWt
# d/0Ugh9/C30fJuWcIQYTZL+O7WmbhmVXohjyO4cY7bkkQ2CImOdEfye5e/oiWGpc
# zK8AYWHjzTWsBf5h43SSRfgUSr/P8O5hpFJkWzcRGnsJAEkUDhcrL2x2Zg7ceKEd
# zOncR7YmuJ67w6ezVutxxg7vvn8t2AH93K3hsy25wjUz9LAZRd7XeqqTbu9oZ1wL
# TbBPIp3g3NJ2ddDLIhmhes3nV7qJ8z/FNfOlk4QthRfn22xgMVrByjKmpPGxcP2o
# VGeAtYXK
# SIG # End signature block
